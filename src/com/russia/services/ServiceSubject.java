package com.russia.services;

import com.codename1.io.*;
import com.codename1.ui.events.ActionListener;
import com.russia.entities.forum.subject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ServiceSubject {

    ArrayList<subject> listSujet = new ArrayList<>();

    public void ajoutSubject(subject sujet){
        ConnectionRequest c = new ConnectionRequest();
        String url = "http://localhost:8000/api/subjects/new?title="+
                sujet.getTitle() +"&content="+ sujet.getContent() + "&etat=" + sujet.getEtat();
        c.setUrl(url);
        c.addResponseListener((e)->{
            String s = new String(c.getResponseData());
        });
        NetworkManager.getInstance().addToQueueAndWait(c);
    }

//////////////////////////////////////////////////////////////////////////////////////////////

    public ArrayList<subject> getListSubject(String json)  {

        ArrayList<subject> listSubject = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> sujet = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) sujet.get("root");
            for (Map<String, Object> obj : list) {
                subject e = new subject();
                float id = Float.parseFloat(obj.get("id").toString());
                e.setId((int) id);
                e.setTitle(obj.get("title").toString());
                e.setContent(obj.get("content").toString());
                float d = Float.parseFloat(obj.get("created").toString());
                Date created = new Date((long) ( d - 3600 ) * 1000 );
                e.setCreated(created);
                if(obj.get("filename")== null){
                    e.setImage(null);
                }else{
                    e.setImage(obj.get("filename").toString());
                }
//                System.out.println("imageeeeeee " + obj.get("filename").toString());
                System.out.println("----------------> "+e);
                listSubject.add(e);
            }
        } catch (IOException ex) {}
        System.out.println("list des sujet " +listSubject);
        return listSubject;
    }


//////////////////////////////////////////////////////////////////////////////////////////////

    public ArrayList<subject> getList2(){
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost:8000/api/subjects/all");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceSubject ser = new ServiceSubject();
                listSujet = ser.getListSubject(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listSujet;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    public ArrayList<subject> findByname(String text){
        subject ss = null;
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost:8000/api/subjects/findTitle/"+ text);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceSubject ser = new ServiceSubject();
                listSujet = ser.getListSubject(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listSujet;
    }
}
