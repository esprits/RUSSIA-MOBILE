package com.russia.services;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.russia.contract.Endpoints;
import com.russia.entities.article.Article;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class ArticleService {

    private String mBaseUrl;

    public ArticleService() {
        mBaseUrl = Endpoints.EndpointEntry.API_BASE_URL;
    }

    public List<Article> getListArticle() throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.ARTICLE, "GET");

        Map<String, Object> response = getStringObjectMap(dataResponse);

        List<Map<String, Object>> articleJson = (List<Map<String, Object>>) response.get("articles");


        return articleJson.stream().map(Article::new).collect(Collectors.toList());
    }


    private byte[] GetResponse(String endPoint, String method) throws IOException {
        ConnectionRequest connection = new ConnectionRequest();
        connection.setUrl(String.format("%s/%s", mBaseUrl, endPoint));

        connection.setPost(false);

        NetworkManager.getInstance().addToQueueAndWait(connection);
        byte[] responseData = connection.getResponseData();
        if (responseData == null) {
            throw new IOException("Network Err");
        }

        return responseData;
    }

    private Map<String, Object> getStringObjectMap(byte[] data) throws IOException {
        JSONParser parser = new JSONParser();
        return parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
    }

}
