package com.russia.services.match;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.russia.entities.match.Match;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MatchService {

    public List<Match> getListMatch() {
        ArrayList<Match> listMatch = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://127.0.0.1:8000/api/schedule");
        con.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(con);
        byte[] data = con.getResponseData();
        try {
            Map<String, Object> matchs = getStringObjectMap(data);
            List<Map<String, Object>> list = (List<Map<String, Object>>) matchs.get("root");
            for (Map<String, Object> obj : list) {
                Match match = new Match(obj);
                listMatch.add(match);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return listMatch;
    }

    protected Map<String, Object> getStringObjectMap(byte[] data) throws IOException {
        JSONParser parser = new JSONParser();
        return parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
    }


}
