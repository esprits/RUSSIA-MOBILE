package com.russia.services.match;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.russia.entities.match.Score;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ScoreService {

    public List<Score> getListScore() {
        ArrayList<Score> listScore = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://127.0.0.1:8000/api/score");
        con.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(con);
        byte[] data = con.getResponseData();
        try {
            Map<String, Object> scores = getStringObjectMap(data);
            List<Map<String, Object>> list = (List<Map<String, Object>>) scores.get("root");
            for (Map<String, Object> obj : list) {
                Score score = new Score(obj);
                listScore.add(score);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return listScore;
    }

    protected Map<String, Object> getStringObjectMap(byte[] data) throws IOException {
        JSONParser parser = new JSONParser();
        return parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
    }
}
