package com.russia.services.match;

import com.russia.common.BaseDao;
import com.russia.common.BaseService;
import com.russia.contract.Endpoints;
import com.russia.entities.player.Player;
import com.russia.manager.EntityManager;
import com.russia.manager.PlayerManager;

import java.io.IOException;
import java.util.List;

public class ViewService extends BaseService implements BaseDao<Player> {

    EntityManager<Player> entityManager;

    public ViewService() {
        super();
        entityManager = new PlayerManager();
    }

    public ViewService(String baseUrl) {
        super(baseUrl);
        entityManager = new PlayerManager();
    }


    @Override
    public List<Player> getAll() throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.VIEWS, "GET");
        return entityManager.getAll(dataResponse);

    }

    @Override
    public Player get(int id) throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.VIEWS + SEPRATOR + id, "GET");
        return entityManager.get(dataResponse);
    }
}
