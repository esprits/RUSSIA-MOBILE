package com.russia.services.match;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.russia.entities.match.Event;
import com.russia.entities.match.Score;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EventService {



    public List<Event> getListEventByMatch(Score score){

        ArrayList<Event> listEvent = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://127.0.0.1:8000/api/event/" + score.getMatch().getId());
        con.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(con);
        byte[] data = con.getResponseData();
        try {
            Map<String, Object> events = getStringObjectMap(data);
            List<Map<String, Object>> list = (List<Map<String, Object>>) events.get("root");
            for (Map<String, Object> obj : list) {
                Event event = new Event(obj);
                listEvent.add(event);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return listEvent;
    }

    protected Map<String, Object> getStringObjectMap(byte[] data) throws IOException {
        JSONParser parser = new JSONParser();
        return parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
    }
}
