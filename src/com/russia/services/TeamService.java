package com.russia.services;

import com.codename1.io.JSONParser;
import com.russia.common.BaseDao;
import com.russia.common.BaseService;
import com.russia.contract.Endpoints;
import com.russia.entities.team.Team;
import com.russia.manager.EntityManager;
import com.russia.manager.TeamManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

public class TeamService extends BaseService implements BaseDao<Team> {

    EntityManager<Team> entityManager;

    public TeamService() {
        super();
        entityManager = new TeamManager();
    }

    public TeamService(String baseUrl) {
        super(baseUrl);
        entityManager = new TeamManager();
    }


    @Override
    public List<Team> getAll()  {
        byte[] dataResponse = new byte[0];
        try {
            dataResponse = GetResponse(Endpoints.EndpointEntry.TEAMS, "GET");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            return entityManager.getAll(dataResponse);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public Team get(int id)  {
        byte[] dataResponse = new byte[0];
        try {
            dataResponse = GetResponse(Endpoints.EndpointEntry.TEAMS + SEPRATOR + id, "GET");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            return entityManager.get(dataResponse);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Map<String, Object> getStat(int id)  {
        byte[] dataResponse = new byte[0];
        try {
            dataResponse = GetResponse(Endpoints.EndpointEntry.TEAMS + SEPRATOR +"stat"+ SEPRATOR+ id, "GET");
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONParser parser = new JSONParser();
        try {
            return (Map<String, Object>) parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(dataResponse), "UTF-8")).get("team");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }
}
