package com.russia.services;

import com.russia.common.BaseDao;
import com.russia.common.BaseService;
import com.russia.contract.Endpoints;
import com.russia.entities.player.Player;
import com.russia.manager.EntityManager;
import com.russia.manager.PlayerManager;

import java.io.IOException;
import java.util.List;

public class SkillService extends BaseService implements BaseDao<Player> {

    EntityManager<Player> entityManager;

    public SkillService() {
        super();
        entityManager = new PlayerManager();
    }

    public SkillService(String baseUrl) {
        super(baseUrl);
        entityManager = new PlayerManager();
    }


    @Override
    public List<Player> getAll() throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.SKILLS, "GET");
        return entityManager.getAll(dataResponse);

    }

    @Override
    public Player get(int id) throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.SKILLS + SEPRATOR + id, "GET");
        return entityManager.get(dataResponse);
    }
}
