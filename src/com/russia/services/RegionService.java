package com.russia.services;

import com.russia.common.BaseDao;
import com.russia.common.BaseService;
import com.russia.contract.Endpoints;
import com.russia.entities.guide.Region;
import com.russia.manager.EntityManager;
import com.russia.manager.RegionManager;

import java.io.IOException;
import java.util.List;

public class RegionService extends BaseService implements BaseDao<Region> {

    EntityManager<Region> entityManager;

    public RegionService() {
        super();
        entityManager = new RegionManager();
    }

    public RegionService(String baseUrl) {
        super(baseUrl);
        entityManager = new RegionManager();
    }

    @Override
    public List<Region> getAll() throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.REGIONS, "GET");
        return entityManager.getAll(dataResponse);

    }

    @Override
    public Region get(int id) throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.REGIONS + SEPRATOR + id, "GET");
        return entityManager.get(dataResponse);
    }
}
