package com.russia.services;

import com.codename1.io.*;
import com.codename1.ui.events.ActionListener;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class StatServices {

    public Map<String, Integer> getListNote() {

        Map<String, Integer> listEvenements = new HashMap<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/getvote.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                try {
                    System.out.println((new String(con.getResponseData()).toCharArray()));
                    //renvoi une map avec clé = root et valeur le reste
                    Map<String, Object> note = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    List<Map<String, Object>> list = (List<Map<String, Object>>) note.get("root");

                    for (Map<String, Object> obj : list) {
                        int badge_id = Integer.parseInt((obj.get("badge_id").toString()));
                        String s = obj.get("title").toString();
                        listEvenements.put(s, badge_id);
                    }
                } catch (IOException ex) {
                    System.out.println("erreur parse");
                }
            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);
        return listEvenements;
    }

}