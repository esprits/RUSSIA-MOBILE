package com.russia.services;

import com.codename1.io.*;
import com.codename1.ui.events.ActionListener;
import com.russia.entities.forum.comment;
import com.russia.entities.forum.subject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ServiceComment {
    subject ss ;
    public void ajoutComment(comment comment){
        ConnectionRequest c = new ConnectionRequest();
        String url = "http://localhost:8000/comments/api/new?content=" +
                comment.getContent()+"&id_subject="+comment.getIdSubject();
        c.setUrl(url);
        c.addResponseListener((e)->{
            String s = new String(c.getResponseData());
        });
        NetworkManager.getInstance().addToQueueAndWait(c);
    }

    public ArrayList<comment> getListComment(String json) {

        ArrayList<comment> listComment = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> comment = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) comment.get("root");
            for (Map<String, Object> obj : list) {
                comment e = new comment();
                float id = Float.parseFloat(obj.get("id").toString());
                System.out.println(id);
                e.setId((int) id);
                e.setContent(obj.get("content").toString());
                float d = Float.parseFloat(obj.get("created").toString());
                Date created = new Date((long) ( d - 3600 ) * 1000 );
                e.setCreated(created);
                System.out.println("--------------> "+e);
                listComment.add(e);
            }
        } catch (IOException ex) {}
        return listComment;
    }
    ArrayList<comment> listComments = new ArrayList<>();

    public ArrayList<comment> getList2Comments(subject s){
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost:8000/comments/find/"+s.getId());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceComment ser = new ServiceComment();
                listComments = ser.getListComment(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listComments;
    }

}
