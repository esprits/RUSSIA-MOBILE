package com.russia.services;


import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.russia.entities.Group.Groupe;
import com.russia.entities.team.Team;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class GroupService {

    ArrayList<Groupe> listGroupe = new ArrayList<>();

    public void ajouterRatin(Groupe G) {
        ConnectionRequest con = new ConnectionRequest();
        String URL = "http://127.0.0.1:8000/groupe/all" + G.getRating() + G.getId();


    }

    public ArrayList<Groupe> getListGroup(String json) throws IOException {
        ArrayList<Groupe> listgroup = new ArrayList<>();
        try {
            System.out.println(json);
            JSONParser j = new JSONParser();
            Map<String, Object> groups = j.parseJSON(new CharArrayReader(json.toCharArray()));
            System.out.println(groups);

            List<Map<String, Object>> list = (List<Map<String, Object>>) groups.get("groups");
            for (Map<String, Object> obj : list) {

                Groupe g = new Groupe();
                float id = Float.parseFloat(obj.get("id").toString());
                System.out.println(id);
                g.setId((int) id);

                g.setNameGroup(obj.get("groupName").toString());

                g.setId_team1((Team) obj.get("Team1"));
                g.setId_team2((Team) obj.get("Team2"));
                g.setId_team3((Team) obj.get("Team3"));
                g.setId_team4((Team) obj.get("Team4"));

                System.out.println(g);

                Map<String, Object> listTeam = null;
                if (obj.get("team1") != null) {
                    listTeam = (Map<String, Object>) obj.get("team1");

                    Team team1 = new Team();
                    team1.setTeamName(listTeam.get("teamName").toString());
                    team1.setTeamLogo(listTeam.get("teamLogo").toString());
                    g.setId_team1(team1);
                }
                if (obj.get("team2") != null) {
                    listTeam = (Map<String, Object>) obj.get("team2");
                    Team team2 = new Team();
                    team2.setTeamName(listTeam.get("teamName").toString());
                    team2.setTeamLogo(listTeam.get("teamLogo").toString());

                    g.setId_team2(team2);
                }
                if (obj.get("team3") != null) {
                    listTeam = (Map<String, Object>) obj.get("team3");
                    Team team3 = new Team();
                    team3.setTeamName(listTeam.get("teamName").toString());
                    team3.setTeamLogo(listTeam.get("teamLogo").toString());

                    g.setId_team3(team3);
                }
                if (obj.get("team4") != null) {
                    listTeam = (Map<String, Object>) obj.get("team4");
                    Team team4 = new Team();
                    team4.setTeamName(listTeam.get("teamName").toString());
                    team4.setTeamLogo(listTeam.get("teamLogo").toString());

                    g.setId_team4(team4);

                }

                listgroup.add(g);

            }

        } catch (IOException ex) {

        }
        System.out.println(listgroup);
        return listgroup;
    }

    public ArrayList<Groupe> getList2() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://127.0.0.1:8000/api/groups");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                GroupService ser = new GroupService();
                try {
                    listGroupe = ser.getListGroup(new String(con.getResponseData()));
                } catch (IOException ex) {
                    // Logger.getLogger(ServiceGroup.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listGroupe;
    }

}
