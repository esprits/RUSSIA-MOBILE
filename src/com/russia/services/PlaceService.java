package com.russia.services;

import com.russia.common.BaseDao;
import com.russia.common.BaseService;
import com.russia.contract.Endpoints;
import com.russia.entities.guide.Place;
import com.russia.manager.EntityManager;
import com.russia.manager.PlaceManager;

import java.io.IOException;
import java.util.List;

public class PlaceService extends BaseService implements BaseDao<Place> {

    EntityManager<Place> entityManager;

    public PlaceService() {
        super();
        entityManager = new PlaceManager();
    }

    public PlaceService(String baseUrl) {
        super(baseUrl);
        entityManager = new PlaceManager();
    }


    @Override
    public List<Place> getAll() throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.PLACES, "GET");
        return entityManager.getAll(dataResponse);

    }

    @Override
    public Place get(int id) throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.PLACES + SEPRATOR + id, "GET");
        return entityManager.get(dataResponse);
    }
}
