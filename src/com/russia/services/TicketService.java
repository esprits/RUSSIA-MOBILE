package com.russia.services;

import com.russia.common.BaseDao;
import com.russia.common.BaseService;
import com.russia.contract.Endpoints;
import com.russia.entities.guide.Place;
import com.russia.entities.guide.Region;
import com.russia.entities.ticket.Ticket;
import com.russia.manager.EntityManager;
import com.russia.manager.RegionManager;
import com.russia.manager.TicketManager;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class TicketService extends BaseService {

    EntityManager<Ticket> entityManager;


    public TicketService() {
        super();
        entityManager = new TicketManager();
    }

    public TicketService(String baseUrl) {
        super(baseUrl);
        entityManager = new TicketManager();
    }

    public Ticket get(int id) throws IOException {
        byte[] dataResponse = GetResponse(Endpoints.EndpointEntry.TICKET + SEPRATOR + id + SEPRATOR + "match", "GET");
        return entityManager.get(dataResponse);
    }
}
