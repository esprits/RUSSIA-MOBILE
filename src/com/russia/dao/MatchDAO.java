package com.russia.dao;

import com.codename1.db.Cursor;
import com.codename1.db.Row;
import com.russia.entities.match.Match;
import com.russia.entities.match.Team;
import com.russia.utils.DataBaseManager;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MatchDAO {

    public void addMatchToFavorite(Match match) {
        if (!exist(match.getId())) {
            TeamDAO teamDAO = new TeamDAO();
            teamDAO.addTeam(match.getFirstTeam());
            teamDAO.addTeam(match.getSecondTeam());

            String params[] = {String.valueOf(match.getId()), String.valueOf(match.getFirstTeam().getId()),
                    String.valueOf(match.getSecondTeam().getId()), String.valueOf(match.getStadium()), String.valueOf(match.getLevel()),
                    new SimpleDateFormat("d-MM-yyyy").format(match.getDates()),
                    String.valueOf(match.getTime())};
            try {
                DataBaseManager.getConnexion().execute("insert into Match (id,firstTeam,secondTeam,stadium,level,date,time) values(?,?,?,?,?,?,?)", params);
            } catch (IOException e) {
            } finally {
                try {
                    DataBaseManager.getDatabase().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<Match> getFavoriteMatch() {
        ArrayList<Match> listMatch = new ArrayList<>();
        TeamDAO teamDAO = new TeamDAO();
        try {
            Cursor cursor = DataBaseManager.getConnexion().executeQuery("select * from Match");
            while (cursor.next()) {
                Row r = cursor.getRow();
                Team firstTeam = teamDAO.getTeamById(r.getInteger(1));
                Team secondTeam = teamDAO.getTeamById(r.getInteger(2));
                try {
                    Date date = (new SimpleDateFormat("d-MM-yyyy").parse(r.getString(5)));
                    listMatch.add(new Match(r.getInteger(0), firstTeam, secondTeam, date, r.getString(6),
                            r.getString(4), r.getString(3)));
                } catch (ParseException e) {
                }
            }
        } catch (IOException e) {

        }
        return listMatch;
    }

    public void deleteMatch(Match match) {
        try {
            DataBaseManager.getConnexion().execute("delete from Match where id = ?", match.getId());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                DataBaseManager.getDatabase().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Boolean exist(int id) {
        Boolean exist = false;
        try {
            Cursor cursor = DataBaseManager.getConnexion().executeQuery("select * from Match where id = ?", id);
            if (cursor.next()) {
                exist = true;
            }
        } catch (IOException e) {
        } finally {
            try {
                DataBaseManager.getDatabase().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return exist;
    }
}
