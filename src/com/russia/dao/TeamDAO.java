package com.russia.dao;

import com.codename1.db.Cursor;
import com.codename1.db.Row;
import com.codename1.io.Util;
import com.russia.entities.match.Team;
import com.russia.utils.DataBaseManager;

import java.io.IOException;
import java.util.ArrayList;

public class TeamDAO {

    public void addTeam(Team team) {
        if (getTeamById(team.getId()) == null) {
            String params[] = {String.valueOf(team.getId()), team.getTeamShortcut(), team.getTeamLogo()};
            try {
                DataBaseManager.getConnexion().execute("insert into Team (id,shortcut,logo) values(?,?,?)", params);
            } catch (IOException e) {

            }finally {
                try {
                    DataBaseManager.getDatabase().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public Team getTeamById(int id) {
        Team team = new Team();
        try {
            Cursor cursor = DataBaseManager.getConnexion().executeQuery("select * from Team where id = ?", id);
            if (cursor.next()) {
                Row r = cursor.getRow();
                team.setId(r.getInteger(0));
                team.setTeamShortcut(r.getString(1));
                team.setTeamLogo(r.getString(2));
            } else {
                team = null;
            }
        } catch (IOException e) {
        }finally {
            try {
                DataBaseManager.getDatabase().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return team;
    }

    public ArrayList<Team> getTeams() {
        ArrayList<Team> listTeam = new ArrayList<>();
        try {
            Cursor cursor = DataBaseManager.getConnexion().executeQuery("select * from Team");
            while (cursor.next()) {
                Row r = cursor.getRow();
                listTeam.add(new Team(r.getInteger(0), r.getString(1), r.getString(2)));
            }
        } catch (IOException e) {
        }
        return listTeam;
    }

}
