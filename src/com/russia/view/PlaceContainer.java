package com.russia.view;

import com.codename1.components.ImageViewer;
import com.codename1.components.ToastBar;
import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.russia.entities.guide.Place;
import com.russia.gui.guide.PlaceFavouriteForm;
import com.russia.gui.guide.PlaceItemForm;
import com.russia.utils.PrefsUtils;

import static com.codename1.charts.util.ColorUtil.rgb;

public class PlaceContainer extends Container {

    private boolean close = false;
    private Place place;


    public PlaceContainer() {

    }

    public PlaceContainer(Place place) {
        this.place = place;
        init();
    }

    public PlaceContainer(Place place, boolean close) {
        this.place = place;
        this.close = close;
        init();
    }


    private void init() {
        setLayout(BoxLayout.y());
        containerStyle();

        Container upperHolder = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Container placeContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));

        Label placeLabel = new Label(place.getName().toUpperCase());
        Label categoryLabel = new Label((place.getType() != null) ? place.getType().toUpperCase() : "");
        placeLabel.getAllStyles().setFgColor(rgb(158, 158, 158));
        placeContainer.add(BorderLayout.CENTER, categoryLabel);

        if (this.close) {
            Label label = new Label();
            Font materialFont = FontImage.getMaterialDesignFont();
            int size = Display.getInstance().convertToPixels(6, true);
            materialFont = materialFont.derive(size, Font.STYLE_PLAIN);
            label.setIcon(FontImage.createFixed(String.valueOf(FontImage.MATERIAL_CLOSE), materialFont, rgb(50, 50, 50), 100, 100));

            placeContainer.add(BorderLayout.EAST, label);
            label.addPointerPressedListener(evt -> {
                if (Dialog.show("Do you want to delete", "", "Submit", "Cancel")) {
                    PrefsUtils.removeValue(String.valueOf(place.getId()));
                    PlaceFavouriteForm placeFavouriteForm = new PlaceFavouriteForm();
                    placeFavouriteForm.getForm().show();
                }
            });
        }
        Container typeContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        typeContainer.add(BorderLayout.CENTER, placeLabel);

        upperHolder.add(placeContainer);
        upperHolder.add(typeContainer);

        Container lowerHolder = new Container(new BorderLayout());
        ((BorderLayout) lowerHolder.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);

        Container imageContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        imageContainer.getStyle().setMargin(30, 30, 50, 30);
        imageContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        imageContainer.getStyle().setPadding(30, 30, 30, 30);

        EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(Display.getInstance().getDisplayWidth() / 3, 400, 0xffff0000), true);
        URLImage placeImage = URLImage.createToStorage(placeholder, place.getPreviewPicture(),
                place.getPreviewPicture());

        imageContainer.add(new ImageViewer(placeImage));
        imageContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));


        Button discover = new Button("DISCOVER");
        discover.getAllStyles().setBorder(Border.createLineBorder(1, rgb(220, 220, 220)));
        discover.getAllStyles().setMargin(0, 2, 10, 10);
        discover.getAllStyles().setFgColor(rgb(240, 0, 0));


        discover.addActionListener(evt -> {
            PlaceItemForm placeItemForm = new PlaceItemForm(place, getComponentForm());
            placeItemForm.getForm().show();
        });


        lowerHolder.add(BorderLayout.CENTER, imageContainer);
        lowerHolder.add(BorderLayout.SOUTH, discover);

        add(upperHolder);
        add(lowerHolder);
    }

    private void containerStyle() {
        getStyle().setBgColor(rgb(255, 255, 255));
        getStyle().setBgTransparency(255);
        getStyle().setMargin(20, 10, 20, 20);
    }
}
