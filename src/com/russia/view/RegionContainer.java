package com.russia.view;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.plaf.Border;
import com.russia.entities.guide.Region;
import com.russia.gui.guide.PlacesForm;

import static com.codename1.charts.util.ColorUtil.rgb;

public class RegionContainer extends Container {
    private static final String URL = "http://localhost:8000/uploads/region/";
    private Region region;

    public RegionContainer() {

    }

    public RegionContainer(Region region) {
        this.region = region;
        init();
    }

    private void init() {
        this.setLayout(new BorderLayout());
        containerStyle();
        SpanLabel label = new SpanLabel(region.getName());

        addComponent(BorderLayout.NORTH, label);
        EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(200, 300, 0xffff0000), true);
        URLImage background = URLImage.createToStorage(placeholder, region.getImage(),
                URL + region.getImage());
        ImageViewer image = new ImageViewer(background);

        addComponent(BorderLayout.CENTER, image);

        Button button = new Button("visit".toUpperCase());
        button.getAllStyles().setBorder(Border.createLineBorder(1, rgb(220,220,220)));
        button.getAllStyles().setFgColor(rgb(240,0,0));
        button.getAllStyles().setMargin(2, 2, 0, 0);

        button.addActionListener(evt -> {
            PlacesForm form = new PlacesForm(this.region, getComponentForm());
            form.getForm().show();
        });

        addComponent(BorderLayout.SOUTH, button);


        setLeadComponent(button);
        label.getAllStyles().setFgColor(rgb(255, 255, 255));
        label.addPointerPressedListener(evt -> {

        });
    }

    private void containerStyle() {
        getStyle().setBgColor(rgb(255, 255, 255));
        getStyle().setBgTransparency(255);
        getStyle().setMargin(20, 20, 20, 20);
    }
}

