package com.russia.view;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.plaf.Border;
import com.russia.entities.guide.Place;
import com.russia.gui.guide.PlaceItemForm;

import static com.codename1.charts.util.ColorUtil.rgb;

public class FavouriteContainer extends Container {
    private Place place;

    public FavouriteContainer(Place place) {
        this.place = place;
        init();
    }

    private void init() {
        this.setLayout(new BorderLayout());
        containerStyle();
        SpanLabel label = new SpanLabel(trim(place.getName()));

        addComponent(com.codename1.ui.layouts.BorderLayout.NORTH, label);
        EncodedImage placeholder = EncodedImage.createFromImage(com.codename1.ui.Image.createImage(200, 300, 0xffff0000), true);
        URLImage background = URLImage.createToStorage(placeholder, place.getPreviewPicture(),
                place.getPreviewPicture());
        ImageViewer image = new ImageViewer(background);

        addComponent(com.codename1.ui.layouts.BorderLayout.CENTER, image);

        com.codename1.ui.Button button = new com.codename1.ui.Button("visit".toUpperCase());
        button.getAllStyles().setBorder(Border.createLineBorder(1, rgb(220, 220, 220)));
        button.getAllStyles().setFgColor(rgb(240, 0, 0));
        button.getAllStyles().setMargin(2, 2, 0, 0);

        button.addActionListener(evt -> {
            PlaceItemForm form = new PlaceItemForm(this.place, button.getComponentForm());
            form.getForm().show();
        });

        addComponent(BorderLayout.SOUTH, button);


        setLeadComponent(button);
        label.getAllStyles().setFgColor(rgb(255, 255, 255));
        label.addPointerPressedListener(evt -> {

        });
    }

    private String trim(String name) {
        int maxLength = 20;
        if (name.length() < maxLength) {
            return name;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name.substring(0, maxLength));
        stringBuilder.append(" ...");

        return stringBuilder.toString();
    }


    private void containerStyle() {
        getStyle().setBgColor(rgb(255, 255, 255));
        getStyle().setBgTransparency(255);
        getStyle().setMargin(20, 20, 20, 20);
    }
}
