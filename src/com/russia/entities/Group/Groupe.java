package com.russia.entities.Group;

import com.russia.entities.team.Team;

public class Groupe {


    private int id;
    private String nameGroup;
    private Team id_team1;
    private Team id_team2;
    private Team id_team3;
    private Team id_team4;
    private int rating;

    public Groupe() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Groupe(int id, int rating) {
        this.id = id;
        this.rating = rating;
    }



    public int getId() {
        return id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }



    public Team getId_team1() {
        return id_team1;
    }

    public void setId_team1(Team id_team1) {
        this.id_team1 = id_team1;
    }

    public Team getId_team2() {
        return id_team2;
    }

    public void setId_team2(Team id_team2) {
        this.id_team2 = id_team2;
    }

    public Team getId_team3() {
        return id_team3;
    }

    public void setId_team3(Team id_team3) {
        this.id_team3 = id_team3;
    }

    public Team getId_team4() {
        return id_team4;
    }

    public void setId_team4(Team id_team4) {
        this.id_team4 = id_team4;
    }

    public Groupe(int id, String Name, Team id_team1, Team id_team2, Team id_team3, Team id_team4) {
        this.id = id;
        this.nameGroup = nameGroup;
        this.id_team1 = id_team1;
        this.id_team2 = id_team2;
        this.id_team3 = id_team3;
        this.id_team4 = id_team4;
    }

    @Override
    public String toString() {
        return "Groupe{" + "id=" + id + ", nameGroup=" + nameGroup + ", id_team1=" + id_team1 + ", id_team2=" + id_team2 + ", id_team3=" + id_team3 + ", id_team4=" + id_team4 + '}';
    }


}
