package com.russia.entities.Group;

public class Rating {


    private User id_user;
    private Groupe id_groupe;
    private int ratingValue;

    public int getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(int ratingValue) {
        this.ratingValue = ratingValue;
    }

    public Rating() {
    }

   public User getId_user() {
        return id_user;
    }

    public void setId_user(User id_user) {
        this.id_user = id_user;
    }

    public Groupe getId_groupe() {
        return id_groupe;
    }

    public void setId_groupe(Groupe id_groupe) {
        this.id_groupe = id_groupe;
    }





}
