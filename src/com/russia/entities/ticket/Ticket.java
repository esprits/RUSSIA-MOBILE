package com.russia.entities.ticket;

import com.russia.common.BaseModel;
import com.russia.contract.PlaceContract;
import com.russia.contract.TicketContract;
import com.russia.entities.match.Match;
import com.russia.utils.IdentityFormatter;

import java.util.Map;

public class Ticket extends BaseModel {
    private int id;
    private double price;
    private double quantity;

    private Match match;

    public Ticket(double price, double quantity) {

        this.price = price;
        this.quantity = quantity;
    }

    public Ticket(Map<String, Object> json) {
        super(json);
    }

    public Ticket() {

    }


    @Override
    public String assemble() {
        return getColumnList(TicketContract.TicketEntry.COLUMNS);
    }

    @Override
    public void resolve(Map<String, Object> json) {
        parse(json);
    }


    @Override
    public void parse(Map<String, Object> json) {
        // todo
        this.id = IdentityFormatter.parse(String.valueOf(json.get(TicketContract.TicketEntry._ID)));
        this.quantity = (double) json.get(TicketContract.TicketEntry.COLUMN_NAME_QUANTITY);
        this.price = Double.valueOf((String) json.get(TicketContract.TicketEntry.COLUMN_NAME_PRICE));
    }

    private void createMatch(Map<String, Object> regionJson) {
    }

    public int getId() {
        return id;
    }

    public Ticket setId(int id) {
        this.id = id;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Ticket setPrice(double price) {
        this.price = price;
        return this;
    }

    public double getQuantity() {
        return quantity;
    }

    public Ticket setQuantity(double quantity) {
        this.quantity = quantity;
        return this;
    }

    @Override
    public String toString() {
        return String.format("Ticket{id=%d, price=%s, quantity=%d}", id, price, quantity);
    }

    public Match getMatch() {
        return match;
    }

    public Ticket setMatch(Match match) {
        this.match = match;
        return this;
    }

//    public Match getMatch() {
//        return match;
//    }
//
//    public Ticket setMatch(Match match) {
//        this.match = match;
//        return this;
//    }
}
