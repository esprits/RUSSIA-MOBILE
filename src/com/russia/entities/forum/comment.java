package com.russia.entities.forum;

import java.util.Date;

public class comment {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String content;
    private Date created;
    private Date updated;
    private subject idSubject;

    public comment() {
    }

    public comment(Integer id, String content, Date created, Date updated, subject idSubject) {
        this.id = id;
        this.content = content;
        this.created = created;
        this.updated = updated;
        this.idSubject = idSubject;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }


    public subject getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(subject idSubject) {
        this.idSubject = idSubject;
    }


    @Override
    public String toString() {
        return "entities.Comment[ id=" + id + " ]";
    }
}

