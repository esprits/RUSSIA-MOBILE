package com.russia.entities.forum;

import java.util.Collection;
import java.util.Date;

public class subject {
    private Collection<comment> commentCollection;
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String title;
    private String content;
    private String image;
    private String etat;
    private Date created;
    private Date updated;

    public subject() {
    }

    public subject(Integer id) {
        this.id = id;
    }

    public subject(Integer id, String title, String content, String image, String etat, Date created, Date updated) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.image = image;
        this.etat = etat;
        this.created = created;
        this.updated = updated;
    }



    public subject(Integer id, String title, String content, String image, String etat) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.image = image;
        this.etat = etat;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }


    @Override
    public String toString() {
        return "entities.Subject[ id=" + id + " ]";
    }

    public Collection<comment> getCommentCollection() {
        return commentCollection;
    }

    public void setCommentCollection(Collection<comment> commentCollection) {
        this.commentCollection = commentCollection;
    }
}
