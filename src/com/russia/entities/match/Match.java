package com.russia.entities.match;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class Match {
    private int id;
    private Team firstTeam;
    private Team secondTeam;
    private Date dates;
    private String time;
    private String level;
    private String stadium;
    private boolean played;


    public Match(Map<String, Object> jsonObj) {
        parse(jsonObj);
    }

    public Match(int id, Team firstTeam, Team secondTeam, Date dates, String time, String level, String stadium, boolean played) {
        this.id = id;
        this.firstTeam = firstTeam;
        this.secondTeam = secondTeam;
        this.dates = dates;
        this.time = time;
        this.level = level;
        this.stadium = stadium;
        this.played = played;
    }

    public Match(int id, Team firstTeam, Team secondTeam, Date dates, String time, String level, String stadium) {
         this.id = id;
        this.firstTeam = firstTeam;
        this.secondTeam = secondTeam;
        this.dates = dates;
        this.time = time;
        this.level = level;
        this.stadium = stadium;
        //this.played = played;
    }

    private void parse(Map<String, Object> jsonObj) {
        this.id = (int) Float.parseFloat(jsonObj.get("id").toString());
        try {
            this.dates = new SimpleDateFormat(("d-MM-yyyy")).parse(jsonObj.get("date").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.time = jsonObj.get("time").toString();
        this.level = jsonObj.get("level").toString();
        this.stadium = jsonObj.get("stadium").toString();
        this.firstTeam = new Team ((Map<String, Object>) jsonObj.get("firstTeam"));
        this.secondTeam = new Team((Map<String, Object>)jsonObj.get("secondTeam"));
    }

    public Match() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Team getFirstTeam() {
        return firstTeam;
    }

    public void setFirstTeam(Team firstTeam) {
        this.firstTeam = firstTeam;
    }

    public Team getSecondTeam() {
        return secondTeam;
    }

    public void setSecondTeam(Team secondTeam) {
        this.secondTeam = secondTeam;
    }

    public Date getDates() {
        return dates;
    }

    public void setDates(Date dates) {
        this.dates = dates;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public boolean isPlayed() {
        return played;
    }

    public void setPlayed(boolean played) {
        this.played = played;
    }

    @Override
    public String toString() {
        return "match{" +
                "id=" + id +
                ", firstTeam=" + firstTeam +
                ", secondTeam=" + secondTeam +
                ", dates=" + dates +
                ", time='" + time + '\'' +
                ", level='" + level + '\'' +
                ", stadium='" + stadium + '\'' +
                ", played=" + played +
                '}';
    }


}
