package com.russia.entities.match;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Team {
    private int id;
    private String teamName;
    private String teamShortcut;
    private String teamLogo;
    private int matchWon;
    private int matchLost;
    private int matchDraw;
    private int goalScored;
    private int goalIn;
    private int participation;
    private int winner;
    private int second;
    private int third;
    private List<Player> players;

    public Team (Map<String, Object> jsonObj){
        parse(jsonObj);
    }

    public Team(int id, String shortcut, String logo) {
        this.id = id;
        this.teamShortcut = shortcut;
        this.teamLogo = logo;

    }

    private void parse(Map<String, Object> jsonObj){
        this.id = (int) Float.parseFloat(jsonObj.get("id").toString());
        this.teamName = jsonObj.get("name").toString();
        this.teamShortcut = jsonObj.get("shortcut").toString();
        this.teamLogo = jsonObj.get("logo").toString();

    }

    public Team() {
        this.matchWon = 0;
        this.matchLost = 0;
        this.matchDraw = 0;
        this.goalScored = 0;
        this.goalIn = 0;
        players = new ArrayList<Player>();
    }

    public Team(int id, String teamName, String teamShortcut, String teamLogo, int matchWon, int matchLost, int matchDraw, int goalScored, int goalIn, int participation, int winner, int second, int third) {
        this.id = id;
        this.teamName = teamName;
        this.teamShortcut = teamShortcut;
        this.teamLogo = teamLogo;
        this.matchWon = matchWon;
        this.matchLost = matchLost;
        this.matchDraw = matchDraw;
        this.goalScored = goalScored;
        this.goalIn = goalIn;
        this.participation = participation;
        this.winner = winner;
        this.second = second;
        this.third = third;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamShortcut() {
        return teamShortcut;
    }

    public String getTeamLogo() {
        return teamLogo;
    }

    public void setTeamLogo(String teamLogo) {
        this.teamLogo = teamLogo;
    }

    public void setTeamShortcut(String teamShortcut) {
        this.teamShortcut = teamShortcut;
    }

    public int getMatchWon() {
        return matchWon;
    }

    public void setMatchWon(int matchWon) {
        this.matchWon = matchWon;
    }

    public int getMatchLost() {
        return matchLost;
    }

    public void setMatchLost(int matchLost) {
        this.matchLost = matchLost;
    }

    public int getMatchDraw() {
        return matchDraw;
    }

    public void setMatchDraw(int matchDraw) {
        this.matchDraw = matchDraw;
    }

    public int getGoalScored() {
        return goalScored;
    }

    public void setGoalScored(int goalScored) {
        this.goalScored = goalScored;
    }

    public int getGoalIn() {
        return goalIn;
    }

    public void setGoalIn(int goalIn) {
        this.goalIn = goalIn;
    }

    public int getParticipation() {
        return participation;
    }

    public void setParticipation(int participation) {
        this.participation = participation;
    }

    public int getWinner() {
        return winner;
    }

    public void setWinner(int winner) {
        this.winner = winner;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getThird() {
        return third;
    }

    public void setThird(int third) {
        this.third = third;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", teamName='" + teamName + '\'' +
                ", teamShortcut='" + teamShortcut + '\'' +
                ", teamLogo='" + teamLogo +"}";

    }
}
