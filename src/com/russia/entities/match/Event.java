package com.russia.entities.match;


import java.sql.Time;
import java.util.Map;

public class Event {

    private int id;
    private Match match;
    private int idTeam;
    private int idPlayer;
    private int minutes;
    private Time times;
    private String typeEvent;
    private String description;

    public Event(Map<String, Object> jsonObject) {
        this.id = (int) Float.parseFloat(jsonObject.get("id").toString());
        //   this.match = new Match((Map<String, Object>) jsonObject.get("match"));
        this.minutes = (int) Float.parseFloat(jsonObject.get("minutes").toString());
        this.typeEvent = jsonObject.get("typeEvent").toString();
        if (jsonObject.get("description") == null) {
            this.description = "";
        } else {
            this.description = jsonObject.get("description").toString();
        }
        this.idTeam = (int) Float.parseFloat(jsonObject.get("idTeam").toString());

    }

    public Event(Match match, int idTeam, int idPlayer, int minutes, Time times, String typeEvent, String description) {
        this.match = match;
        this.idTeam = idTeam;
        this.idPlayer = idPlayer;
        this.minutes = minutes;
        this.times = times;
        this.typeEvent = typeEvent;
        this.description = description;
    }

    public Event() {

    }

    public Event(int id, Match match, int idTeam, int idPlayer, int minutes, Time times, String typeEvent, String description) {
        this.id = id;
        this.match = match;
        this.idTeam = idTeam;
        this.idPlayer = idPlayer;
        this.minutes = minutes;
        this.times = times;
        this.typeEvent = typeEvent;
        this.description = description;
    }

    public Match getMatch() {
        return match;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(int idTeam) {
        this.idTeam = idTeam;
    }

    public int getIdPlayer() {
        return idPlayer;
    }

    public void setIdPlayer(int idPlayer) {
        this.idPlayer = idPlayer;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public Time getTimes() {
        return times;
    }

    public void setTimes(Time times) {
        this.times = times;
    }

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    @Override
    public String toString() {
        return "\nEvent{" +
                "id=" + id +
                ", match=" + match +
                ", idTeam=" + idTeam +
                ", idPlayer=" + idPlayer +
                ", minutes=" + minutes +
                ", times=" + times +
                ", typeEvent=" + typeEvent +
                ", description='" + description + '\'' +
                '}';
    }
}
