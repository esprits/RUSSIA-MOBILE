package com.russia.entities.match;


import java.util.Map;

public class Score {
    private int id;
    private Match match;
    private int scoreTeam1;
    private int scoreTeam2;

    public Score(Map<String, Object> jsonObject){
        this.id = (int) Float.parseFloat(jsonObject.get("id").toString());
        this.match = new Match((Map<String,Object>)jsonObject.get("match"));
        this.scoreTeam1 = (int) Float.parseFloat((jsonObject.get("scoreTeam1").toString()));
        this.scoreTeam2 = (int) Float.parseFloat(jsonObject.get("scoreTeam2").toString());

    }

    public Score(int id, Match match, int scoreTeam1, int scoreTeam2) {
        this.id = id;
        this.match = match;
        this.scoreTeam1 = scoreTeam1;
        this.scoreTeam2 = scoreTeam2;
    }

    public Score(Match match, int scoreTeam1, int scoreTeam2) {

        this.match = match;
        this.scoreTeam1 = scoreTeam1;
        this.scoreTeam2 = scoreTeam2;
    }

    public Score() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Match getMatch() {
        return match;
    }

    public void setMtch(Match match) {
        this.match = match;
    }

    public int getScoreTeam1() {
        return scoreTeam1;
    }

    public void setScoreTeam1(int scoreTeam1) {
        this.scoreTeam1 = scoreTeam1;
    }

    public int getScoreTeam2() {
        return scoreTeam2;
    }

    public void setGetScoreTeam2(int getScoreTeam2) {
        this.scoreTeam2 = getScoreTeam2;
    }



    @Override
    public String toString() {
        return "Score{" +
                "id=" + id +
                ", match=" + match +
                ", scoreTeam1=" + scoreTeam1 +
                ", scoreTeam2=" + scoreTeam2 +
                '}';
    }

}
