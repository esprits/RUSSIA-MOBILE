package com.russia.entities.match;

public class Player {
    private int id;
    private String playerName;
    private String playerLastName;


    public Player(int id, String playerName, String playerLastName) {
        this.id = id;
        this.playerName = playerName;
        this.playerLastName = playerLastName;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerLastName() {
        return playerLastName;
    }

    public void setPlayerLastName(String playerLastName) {
        this.playerLastName = playerLastName;
    }

}