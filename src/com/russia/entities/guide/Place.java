package com.russia.entities.guide;

import com.russia.common.BaseModel;
import com.russia.contract.PlaceContract;
import com.russia.entities.location.Location;
import com.russia.utils.IdentityFormatter;

import java.util.Map;

public class Place extends BaseModel {

    private int id;
    private String type;
    private String name;
    private String information;
    private String previewText;
    private String previewPicture;
    private String phone;
    private String siteUrl;

    private Region region;
    private Location location;
    private Category category;


    public Place(String type, String name, String information, String previewText, String previewPicture, String phone, String siteUrl, Region region, Location location, Category category) {
        this.type = type;
        this.name = name;
        this.information = information;
        this.previewText = previewText;
        this.previewPicture = previewPicture;
        this.phone = phone;
        this.siteUrl = siteUrl;
        this.region = region;
        this.location = location;
        this.category = category;
    }


    public Place(int id, String type, String name, String information, String previewText, String previewPicture, String phone, String siteUrl, Region region, Location location, Category category) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.information = information;
        this.previewText = previewText;
        this.previewPicture = previewPicture;
        this.phone = phone;
        this.siteUrl = siteUrl;
        this.region = region;
        this.location = location;
        this.category = category;
    }

    public Place(Map<String, Object> json) {
        super(json);
    }

    public Place() {

    }

    @Override
    public String assemble() {
        return getColumnList(PlaceContract.PlaceEntry.COLUMNS);
    }

    @Override
    public void resolve(Map<String, Object> json) {
        parse(json);
    }

    @Override
    public void parse(Map<String, Object> json) {
        this.id = IdentityFormatter.parse(String.valueOf(json.get(PlaceContract.PlaceEntry._ID)));
        this.name = (String) json.get(PlaceContract.PlaceEntry.COLUMN_NAME_NAME);
        this.type = (String) json.get(PlaceContract.PlaceEntry.COLUMN_NAME_TYPE);
        this.phone = (String) json.get(PlaceContract.PlaceEntry.COLUMN_NAME_PHONE);
        this.siteUrl = (String) json.get(PlaceContract.PlaceEntry.COLUMN_NAME_URL);
        this.previewPicture = (String) json.get(PlaceContract.PlaceEntry.COLUMN_NAME_PICTURE);
        this.information = (String) json.get(PlaceContract.PlaceEntry.COLUMN_NAME_INFORMATION);

        if (json.containsKey(PlaceContract.PlaceEntry.COLUMN_NAME_LOCATION))
            createLocation((Map<String, Object>) json.get(PlaceContract.PlaceEntry.COLUMN_NAME_LOCATION));
        if (json.containsKey(PlaceContract.PlaceEntry.COLUMN_NAME_CATEGORY))
            createCategory((Map<String, Object>) json.get(PlaceContract.PlaceEntry.COLUMN_NAME_CATEGORY));
        if (json.containsKey(PlaceContract.PlaceEntry.COLUMN_NAME_REGION))
            createRegion((Map<String, Object>) json.get(PlaceContract.PlaceEntry.COLUMN_NAME_REGION));

    }

    private void createRegion(Map<String, Object> regionJson) {
        if (this.region != null)
            this.region = new Region(regionJson);
    }

    private void createCategory(Map<String, Object> categoryJson) {
        this.category = new Category(categoryJson);
    }

    private void createLocation(Map<String, Object> locationJson) {
        this.location = new Location(locationJson);
    }

    public int getId() {
        return id;
    }

    public Place setId(int id) {
        this.id = id;
        return this;
    }

    public String getType() {
        return type;
    }

    public Place setType(String type) {
        this.type = type;
        return this;
    }

    public String getName() {
        return name;
    }

    public Place setName(String name) {
        this.name = name;
        return this;
    }

    public String getInformation() {
        return information;
    }

    public Place setInformation(String information) {
        this.information = information;
        return this;
    }

    public String getPreviewText() {
        return previewText;
    }

    public Place setPreviewText(String previewText) {
        this.previewText = previewText;
        return this;
    }

    public String getPreviewPicture() {
        return previewPicture;
    }

    public Place setPreviewPicture(String previewPicture) {
        this.previewPicture = previewPicture;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Place setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public Place setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
        return this;
    }

    public Region getRegion() {
        return region;
    }

    public Place setRegion(Region region) {
        this.region = region;
        return this;
    }

    public Location getLocation() {
        return location;
    }

    public Place setLocation(Location location) {
        this.location = location;
        return this;
    }

    public Category getCategory() {
        return category;
    }

    public Place setCategory(Category category) {
        this.category = category;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Place{");
        sb.append("id=").append(id);
        sb.append(", type='").append(type).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", information='").append(information).append('\'');
        sb.append(", previewText='").append(previewText).append('\'');
        sb.append(", previewPicture='").append(previewPicture).append('\'');
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", siteUrl='").append(siteUrl).append('\'');
        sb.append(", region=").append(region);
        sb.append(", location=").append(location);
        sb.append(", category=").append(category);
        sb.append('}');
        return sb.toString();
    }
}
