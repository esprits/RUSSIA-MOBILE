package com.russia.entities.guide;

import com.russia.common.BaseModel;
import com.russia.contract.CategoryContract;
import com.russia.utils.IdentityFormatter;

import java.util.Map;

public class Category extends BaseModel {
    public static final String CATEGORY_TYPE_MUSEUM = "²museum";
    public static final String CATEGORY_TYPE_ATTRACTION = "attractions";
    public static final String CATEGORY_TYPE_PARK = "park";
    public static final String CATEGORY_TYPE_ENTERTAINMENT = "entertainment";
    public static final String CATEGORY_TYPE_CAFE = "cafe";
    public static final String CATEGORY_TYPE_SPORT = "sport";
    public static final String CATEGORY_TYPE_RELEGIOUS = "religious";
    public static final String CATEGORY_TYPE_THEATRE = "theatre";


    private int id;
    private String name;
    private String code;
    private String icon;

    /**
     * @param id
     * @param name
     * @param code
     * @param icon
     */
    public Category(int id, String name, String code, String icon) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.icon = icon;
    }


    /**
     * @param name
     * @param code
     * @param icon
     */
    public Category(String name, String code, String icon) {
        this.name = name;
        this.code = code;
        this.icon = icon;
    }

    public Category(Map<String, Object> json) {
        super(json);
    }


    @Override
    public String assemble() {
        return getColumnList(CategoryContract.CategoryEntry.COLUMNS);
    }

    @Override
    public void resolve(Map<String, Object> json) {
        parse(json);
    }

    @Override
    public void parse(Map<String, Object> json) {
        // TODO: 4/25/2018 id parse

        this.id = IdentityFormatter.parse(String.valueOf(json.get(CategoryContract.CategoryEntry._ID)));
        this.name = (String) json.get(CategoryContract.CategoryEntry.COLUMN_NAME_NAME);
        this.code = (String) json.get(CategoryContract.CategoryEntry.COLUMN_NAME_CODE);
        this.icon = (String) json.get(CategoryContract.CategoryEntry.COLUMN_NAME_ICON);
    }

    public int getId() {
        return id;
    }

    public Category setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Category setName(String name) {
        this.name = name;
        return this;
    }

    public String getCode() {
        return code;
    }

    public Category setCode(String code) {
        this.code = code;
        return this;
    }

    public String getIcon() {
        return icon;
    }

    public Category setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Category{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append(", icon='").append(icon).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
