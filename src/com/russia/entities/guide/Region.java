package com.russia.entities.guide;

import com.russia.common.BaseModel;
import com.russia.contract.RegionContract;
import com.russia.entities.location.Location;
import com.russia.utils.IdentityFormatter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Region extends BaseModel {


    private int id;
    private String name;
    private String slug;
    private String image;
    private String file;
    private String youtube;

    // Association
    private Location location;

    private List<Place> places;


    public Region(int id, String name, String image, String file, String youtube, Location location) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.file = file;
        this.youtube = youtube;
        this.location = location;
    }

    public Region(String name, String image, String file, String youtube, Location location) {
        this.name = name;
        this.image = image;
        this.file = file;
        this.youtube = youtube;
        this.location = location;
    }

    public Region(Map<String, Object> json) {
        super(json);
    }

    public Region() {

    }

    @Override
    public String assemble() {
        return getColumnList(RegionContract.RegionEntry.COLUMNS);
    }

    @Override
    public void resolve(Map<String, Object> json) {
        parse(json);
    }

    @Override
    public void parse(Map<String, Object> json) {
        // TODO: 4/25/2018 Parse ID to int make sure that you use FORMATTER

        this.id = IdentityFormatter.parse(String.valueOf(json.get(RegionContract.RegionEntry._ID)));
        this.name = (String) json.get(RegionContract.RegionEntry.COLUMN_NAME_NAME);
        this.slug = (String) json.get(RegionContract.RegionEntry.COLUMN_NAME_SLUG);
        this.image = (String) json.get(RegionContract.RegionEntry.COLUMN_NAME_IMAGE);
        if (json.containsKey(RegionContract.RegionEntry.COLUMN_NAME_PLACES))
            createPlaces((List<Map<String, Object>>) json.get(RegionContract.RegionEntry.COLUMN_NAME_PLACES));
        if (json.containsKey(RegionContract.RegionEntry.COLUMN_NAME_LOCATION))
            createLocation((Map<String, Object>) json.get(RegionContract.RegionEntry.COLUMN_NAME_LOCATION));
    }

    private void createPlaces(List<Map<String, Object>> placesJson) {
        this.places = placesJson.stream().map(o -> {
            Place place = new Place();
            place.setRegion(this);
            place.parse(o);
            return place;
        }).collect(Collectors.toList());
    }

    private void createLocation(Map<String, Object> locationJson) {
        this.location = new Location(locationJson);
    }

    public int getId() {
        return id;
    }

    public Region setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Region setName(String name) {
        this.name = name;
        return this;
    }

    public String getSlug() {
        return slug;
    }

    public Region setSlug(String slug) {
        this.slug = slug;
        return this;
    }

    public String getImage() {
        return image;
    }

    public Region setImage(String image) {
        this.image = image;
        return this;
    }

    public String getFile() {
        return file;
    }

    public Region setFile(String file) {
        this.file = file;
        return this;
    }

    public String getYoutube() {
        return youtube;
    }

    public Region setYoutube(String youtube) {
        this.youtube = youtube;
        return this;
    }

    public Location getLocation() {
        return location;
    }

    public Region setLocation(Location location) {
        this.location = location;
        return this;
    }

    @Override
    public String toString() {
        return name;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public Region setPlaces(List<Place> places) {
        this.places = places;
        return this;
    }
}
