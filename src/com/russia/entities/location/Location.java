package com.russia.entities.location;

import com.russia.common.BaseModel;
import com.russia.contract.LocationContract;
import com.russia.utils.IdentityFormatter;

import java.util.Map;

public class Location extends BaseModel {
    private int id;

    private String longitude;
    private String latitude;
    private String country;
    private String city;
    private String state;
    private String street1;
    private String street2;
    private String postalCode;

    public Location(Map<String, Object> json) {
        super(json);
    }

    public Location() {

    }

    @Override
    public String assemble() {
        return getColumnList(LocationContract.LocationEntry.COLUMNS);
    }

    @Override
    public void resolve(Map<String, Object> json) {
        parse(json);
    }

    @Override
    public void parse(Map<String, Object> json) {

        this.id = IdentityFormatter.parse(String.valueOf(json.get(LocationContract.LocationEntry._ID)));
        this.city = (String) json.get(LocationContract.LocationEntry.COLUMN_NAME_CITY);
        this.country = (String) json.get(LocationContract.LocationEntry.COLUMN_NAME_COUNTRY);
        this.state = (String) json.get(LocationContract.LocationEntry.COLUMN_NAME_STATE);
        this.street1 = (String) json.get(LocationContract.LocationEntry.COLUMN_NAME_STREET1);
        this.street2 = (String) json.get(LocationContract.LocationEntry.COLUMN_NAME_STREET2);
        this.postalCode = (String) json.get(LocationContract.LocationEntry.COLUMN_NAME_POSTAL);
        this.longitude = (String) json.get(LocationContract.LocationEntry.COLUMN_NAME_LONGITUDE);
        this.latitude = (String) json.get(LocationContract.LocationEntry.COLUMN_NAME_LATITUDE);
    }


    public int getId() {
        return id;
    }

    public Location setId(int id) {
        this.id = id;
        return this;
    }

    public String getLongitude() {
        return longitude;
    }

    public Location setLongitude(String longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getLatitude() {
        return latitude;
    }

    public Location setLatitude(String latitude) {
        this.latitude = latitude;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Location setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Location setCity(String city) {
        this.city = city;
        return this;
    }

    public String getState() {
        return state;
    }

    public Location setState(String state) {
        this.state = state;
        return this;
    }

    public String getStreet1() {
        return street1;
    }

    public Location setStreet1(String street1) {
        this.street1 = street1;
        return this;
    }

    public String getStreet2() {
        return street2;
    }

    public Location setStreet2(String street2) {
        this.street2 = street2;
        return this;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public Location setPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }
}
