package com.russia.entities.player;

import com.russia.entities.team.Team;
import com.russia.utils.IdentityFormatter;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Player {
    private int id ;
    private String playerName;
    private String playerLastName;
    private int playerNumber;
    private String playerPosition;
    private String playerImage;
    private Date birthday;
    private double weight;
    private double height;
    private int totalGames;
    private String bio;
    private Team nationalTeam;
    private String team;
    private int goalScored;
    private int shots;
    private int shotsOnTarget;
    private int assists;
    private int passes;
    private int fouls;
    private int penaltyKicks;
    private int cornerKicks;
    private int yellowCards;
    private int redCards;
    private int visits;
    private List<Skill> skills;
    private List<Club> clubs;
    private List<View> views;

    public Player(Map<String,Object> json) {
        parse(json);
    }

    private void parse(Map<String, Object> json) {
        this.id = IdentityFormatter.parse(String.valueOf(json.get("id")));
        this.playerNumber = IdentityFormatter.parse(String.valueOf(json.get("playerNumber")));
        this.totalGames = IdentityFormatter.parse(String.valueOf(json.get("totalGames")));
        this.goalScored = IdentityFormatter.parse(String.valueOf(json.get("goalScored")));
        this.shots = IdentityFormatter.parse(String.valueOf(json.get("shots")));
        this.shotsOnTarget = IdentityFormatter.parse(String.valueOf(json.get("shotsOnTarget")));
        this.assists = IdentityFormatter.parse(String.valueOf(json.get("assists")));
        this.passes = IdentityFormatter.parse(String.valueOf(json.get("passes")));
        this.fouls = IdentityFormatter.parse(String.valueOf(json.get("fouls")));
        this.penaltyKicks = IdentityFormatter.parse(String.valueOf(json.get("penaltyKicks")));
        this.cornerKicks = IdentityFormatter.parse(String.valueOf(json.get("cornerKicks")));
        this.yellowCards = IdentityFormatter.parse(String.valueOf(json.get("yellowCard")));
        try {
            this.birthday = new SimpleDateFormat(("d-MM-yyyy")).parse(json.get("birthday").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.redCards = IdentityFormatter.parse(String.valueOf(json.get("redCard")));
        this.visits = IdentityFormatter.parse(String.valueOf(json.get("visits")));
        this.playerName = (String) json.get("playerName");
        this.playerLastName = (String) json.get("playerLastName");
        this.bio = (String) json.get("bio");
        this.playerPosition = (String) json.get("playerPosition");
        this.team = (String) json.get("team");
        this.playerImage = (String) json.get("playerImage");
        this.height = Double.valueOf(String.valueOf(json.get("height")));
        this.weight = Double.valueOf(String.valueOf(json.get("weight")));
        if (json.containsKey("nationalTeam"))
            this.nationalTeam = createTeam((Map<String, Object>) json.get("nationalTeam"));
        if (json.containsKey("clubs"))
            this.clubs = createClubs((List<Map<String, Object>>) json.get("clubs"));
        if (json.containsKey("skills"))
            this.skills = createSkills((List<Map<String, Object>>) json.get("skills"));
        if (json.containsKey("views"))
            this.views = createViews((List<Map<String, Object>>) json.get("views"));
    }

    private List<View> createViews(List<Map<String, Object>> views) {
        return views.stream().map(View::new).collect(Collectors.toList());
    }

    private List<Skill> createSkills(List<Map<String, Object>> skills) {
        return skills.stream().map(Skill::new).collect(Collectors.toList());
    }

    private List<Club> createClubs(List<Map<String, Object>> clubs) {
        return clubs.stream().map(Club::new).collect(Collectors.toList());
    }

    private Team createTeam(Map<String, Object> nationalTeam) {
        return new Team(nationalTeam);
    }

    public Player() {
        this.goalScored = 0;
        this.shots = 0;
        this.shotsOnTarget = 0;
        this.assists = 0;
        this.passes = 0;
        this.fouls = 0;
        this.penaltyKicks = 0;
        this.cornerKicks = 0;
        this.yellowCards = 0;
        this.redCards = 0;
        this.visits = 0;
        skills = new ArrayList<Skill>();
        clubs = new ArrayList<Club>();
        views = new ArrayList<View>();
    }

    public Player(int id, String playerName, String playerLastName, int playerNumber, String playerPosition, String playerImage, Date birthday, double weight, double height, int totalGames, String bio, Team nationalTeam, String team, int goalScored, int shots, int shotsOnTarget, int assists, int passes, int fouls, int penaltyKicks, int cornerKicks, int yellowCards, int redCards, int visits) {
        this.id = id;
        this.playerName = playerName;
        this.playerLastName = playerLastName;
        this.playerNumber = playerNumber;
        this.playerPosition = playerPosition;
        this.playerImage = playerImage;
        this.birthday = birthday;
        this.weight = weight;
        this.height = height;
        this.totalGames = totalGames;
        this.bio = bio;
        this.nationalTeam = nationalTeam;
        this.team = team;
        this.goalScored = goalScored;
        this.shots = shots;
        this.shotsOnTarget = shotsOnTarget;
        this.assists = assists;
        this.passes = passes;
        this.fouls = fouls;
        this.penaltyKicks = penaltyKicks;
        this.cornerKicks = cornerKicks;
        this.yellowCards = yellowCards;
        this.redCards = redCards;
        this.visits = visits;
    }

    public Player(int id, String playerName, String playerLastName) {
        this.id = id;
        this.playerName = playerName;
        this.playerLastName = playerLastName;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerLastName() {
        return playerLastName;
    }

    public void setPlayerLastName(String playerLastName) {
        this.playerLastName = playerLastName;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    public String getPlayerPosition() {
        return playerPosition;
    }

    public void setPlayerPosition(String playerPosition) {
        this.playerPosition = playerPosition;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getTotalGames() {
        return totalGames;
    }

    public void setTotalGames(int totalGames) {
        this.totalGames = totalGames;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Team getNationalTeam() {
        return nationalTeam;
    }

    public void setNationalTeam(Team nationalTeam) {
        this.nationalTeam = nationalTeam;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public int getGoalScored() {
        return goalScored;
    }

    public void setGoalScored(int goalScored) {
        this.goalScored = goalScored;
    }

    public int getShots() {
        return shots;
    }

    public void setShots(int shots) {
        this.shots = shots;
    }

    public int getShotsOnTarget() {
        return shotsOnTarget;
    }

    public void setShotsOnTarget(int shotsOnTarget) {
        this.shotsOnTarget = shotsOnTarget;
    }

    public int getAssists() {
        return assists;
    }

    public void setAssists(int assists) {
        this.assists = assists;
    }

    public int getPasses() {
        return passes;
    }

    public void setPasses(int passes) {
        this.passes = passes;
    }

    public int getFouls() {
        return fouls;
    }

    public void setFouls(int fouls) {
        this.fouls = fouls;
    }

    public int getPenaltyKicks() {
        return penaltyKicks;
    }

    public void setPenaltyKicks(int penaltyKicks) {
        this.penaltyKicks = penaltyKicks;
    }

    public int getCornerKicks() {
        return cornerKicks;
    }

    public void setCornerKicks(int cornerKicks) {
        this.cornerKicks = cornerKicks;
    }

    public int getYellowCards() {
        return yellowCards;
    }

    public void setYellowCards(int yellowCards) {
        this.yellowCards = yellowCards;
    }

    public int getRedCards() {
        return redCards;
    }

    public void setRedCards(int redCards) {
        this.redCards = redCards;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public List<Club> getClubs() {
        return clubs;
    }

    public void setClubs(List<Club> clubs) {
        this.clubs = clubs;
    }

    public String getPlayerImage() {
        return playerImage;
    }

    public void setPlayerImage(String playerImage) {
        this.playerImage = playerImage;
    }

    public int getVisits() {
        return visits;
    }

    public void setVisits(int visits) {
        this.visits = visits;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", playerName='" + playerName + '\'' +
                ", playerLastName='" + playerLastName + '\'' +
                ", playerNumber=" + playerNumber +
                ", playerPosition='" + playerPosition + '\'' +
                ", playerImage='" + playerImage + '\'' +
                ", birthday=" + birthday +
                ", weight=" + weight +
                ", height=" + height +
                ", totalGames=" + totalGames +
                ", bio='" + bio + '\'' +
                ", nationalTeam=" + nationalTeam +
                ", team='" + team + '\'' +
                ", goalScored=" + goalScored +
                ", shots=" + shots +
                ", shotsOnTarget=" + shotsOnTarget +
                ", assists=" + assists +
                ", passes=" + passes +
                ", fouls=" + fouls +
                ", penaltyKicks=" + penaltyKicks +
                ", cornerKicks=" + cornerKicks +
                ", yellowCards=" + yellowCards +
                ", redCards=" + redCards +
                ", visits=" + visits +
                ", skills=" + skills +
                ", clubs=" + clubs +
                ", views=" + views +
                '}';
    }

    public List<View> getViews() {
        return views;
    }

    public void setViews(List<View> views) {
        this.views = views;
    }
}
