package com.russia.entities.player;

import com.russia.utils.IdentityFormatter;

import java.util.Map;

public class Club {
    private int id;
    private String clubName;
    private int seasonStart;
    private int matchPlayed;
    private int goalScored;
    private Player player;

    public Club(Map<String,Object> json) {
        parse(json);
    }

    private void parse(Map<String, Object> json) {
        this.id = IdentityFormatter.parse(String.valueOf(json.get("id")));
        this.matchPlayed = IdentityFormatter.parse(String.valueOf(json.get("matchPlayed")));
        this.seasonStart = IdentityFormatter.parse(String.valueOf(json.get("seasonStart")));
        this.goalScored = IdentityFormatter.parse(String.valueOf(json.get("goalScored")));
        this.clubName = (String) json.get("clubName");
    }

    public Club() {
    }

    public Club(int id, String clubName, int seasonStart, int matchPlayed, int goalScored, Player player) {
        this.id = id;
        this.clubName = clubName;
        this.seasonStart = seasonStart;
        this.matchPlayed = matchPlayed;
        this.goalScored = goalScored;
        this.player = player;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public int getSeasonStart() {
        return seasonStart;
    }

    public void setSeasonStart(int seasonStart) {
        this.seasonStart = seasonStart;
    }

    public int getMatchPlayed() {
        return matchPlayed;
    }

    public void setMatchPlayed(int matchPlayed) {
        this.matchPlayed = matchPlayed;
    }

    public int getGoalScored() {
        return goalScored;
    }

    public void setGoalScored(int goalScored) {
        this.goalScored = goalScored;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "Club{" +
                "id=" + id +
                ", clubName='" + clubName + '\'' +
                ", seasonStart=" + seasonStart +
                ", matchPlayed=" + matchPlayed +
                ", goalScored=" + goalScored +
                '}';
    }
}
