package com.russia.entities.player;


import com.russia.utils.IdentityFormatter;

import java.util.Map;

public class View {
    private int id;
    private String user;
    private String post;
    private Player player;

    public View(int id, String user, String post, Player player) {
        this.id = id;
        this.user = user;
        this.post = post;
        this.player = player;
    }

    public View(String user, String post, Player player) {
        this.user = user;
        this.post = post;
        this.player = player;
    }

    public View(Map<String,Object> json) {
        parse(json);
    }

    private void parse(Map<String, Object> json) {
        this.id = IdentityFormatter.parse(String.valueOf(json.get("id")));
        this.post = (String) json.get("post");
        this.user = (String) json.get("user");
    }

    public View() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
