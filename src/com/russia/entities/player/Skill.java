package com.russia.entities.player;

import com.russia.utils.IdentityFormatter;

import java.util.Map;

public class Skill {
    private int id;
    private String label;
    private int value;
    private Player player;

    public Skill(Map<String,Object> json) {
        parse(json);
    }

    private void parse(Map<String, Object> json) {
        this.id = IdentityFormatter.parse(String.valueOf(json.get("id")));
        this.value = IdentityFormatter.parse(String.valueOf(json.get("value")));
        this.label = (String) json.get("label");
    }

    public Skill() {
    }

    public int getId() {
        return id;
    }

    public Skill(int id, String label, int value, Player player) {
        this.id = id;
        this.label = label;
        this.value = value;
        this.player = player;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "Skill{" +
                "id=" + id +
                ", label='" + label + '\'' +
                ", value=" + value +
                '}';
    }
}
