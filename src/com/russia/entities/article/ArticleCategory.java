package com.russia.entities.article;


public class ArticleCategory  {
    private int id;
    private String name;


    public ArticleCategory(String name) {
        this.name = name;
    }

//    public ArticleCategory(ResultSet resultSet) {
//        super(resultSet);
//    }
//
//    public ArticleCategory(int id, String name) {
//        this.id = id;
//    }
//
//    @Override
//    public String assemble() {
//        return getColumnList(NewsContract.CategoryEntry.COLUMNS);
//    }
//
//    @Override
//    public void resolve(ResultSet resultSet) {
//        try {
//            this.id = resultSet.getInt(NewsContract.CategoryEntry._ID);
//            this.name = resultSet.getString(NewsContract.CategoryEntry.COLUMN_NAME_NAME);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

    public int getId() {
        return id;
    }

    public ArticleCategory setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ArticleCategory setName(String name) {
        this.name = name;
        return this;
    }

}

