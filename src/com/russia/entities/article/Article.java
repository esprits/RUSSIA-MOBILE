package com.russia.entities.article;



import java.util.Date;
import java.util.Map;

public class Article {


    private int id;
    private String title;
    private String content;
    private Date created;
    private Date updated;
    private String image;
    private String slug;
    private ArticleCategory category;


    public Article(int id, String title, String content, Date created, Date updated, String slug, ArticleCategory category) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.created = created;
        this.updated = updated;
        this.slug = slug;
        this.category = category;
    }

    public Article(String title, String content, Date created, Date updated, String slug) {
        this.title = title;
        this.content = content;
        this.created = created;
        this.updated = updated;
        this.slug = slug;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Article() {

    }

    public Article(Map<String, Object> jsonRoot) {
        resolve(jsonRoot);
    }

    private void resolve(Map<String, Object> json) {
        this.title = (String) json.get("title");
        this.content = (String) json.get("content");
        this.image=(String) json.get("image");
    }
//    public Article(ResultSet resultSet) {
//        super(resultSet);
//    }

//    @Override
//    public String assemble() {
//        return getColumnList(NewsContract.ArticleEntry.COLUMNS);
//    }
//
//    @Override
//    public void resolve(ResultSet resultSet) {
//        try {
//            this.id = resultSet.getInt(NewsContract.ArticleEntry._ID);
//            this.title = resultSet.getString(NewsContract.ArticleEntry.COLUMN_NAME_TITLE);
//            this.content = resultSet.getString(NewsContract.ArticleEntry.COLUMN_NAME_CONTENT);
//            this.created = resultSet.getDate(NewsContract.ArticleEntry.COLUMN_NAME_CREATED);
//            this.updated = resultSet.getDate(NewsContract.ArticleEntry.COLUMN_NAME_UPDATED);
//            this.slug = resultSet.getString(NewsContract.ArticleEntry.COLUMN_NAME_SLUG);
//            createBadge(resultSet.getInt(NewsContract.ArticleEntry.COLUMN_NAME_BADGE));
//            createCategory(resultSet.getInt(NewsContract.ArticleEntry.COLUMN_NAME_CATEGORY));
//        } catch (SQLException ignored) {
//            // ignored
//        }
//    }

//    private void createBadge(int id) throws SQLException {
//        this.badge = new BadgeService("russia").get(id);
//    }
//
//    private void createCategory(int id) throws SQLException {
//        this.category = new ArticleCategoryService("russia").get(id);
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }


    public ArticleCategory getCategory() {
        return category;
    }

    public void setCategory(ArticleCategory category) {
        this.category = category;
    }
}

