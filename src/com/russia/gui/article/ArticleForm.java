package com.russia.gui.article;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.*;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.article.Article;
import com.russia.services.ArticleService;

import java.io.IOException;
import java.util.Date;
import java.util.List;


public class ArticleForm {
    Form form;
    private Resources theme;

    ArticleService articleService = new ArticleService();


    public ArticleForm() {
        theme = UIManager.initFirstTheme("/theme");
        form = new Form("News", new BoxLayout(BoxLayout.Y_AXIS));
        form.setScrollable(true);
//        form.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//                Login lolo = new Login();
//                lolo.getF().show();
//            }
//        });




        try {
            System.out.println(articleService.getListArticle());
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        List<Article> articles = null;
        try {
            articles = articleService.getListArticle();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        for (Article article : articles) {

            System.out.println(article.getTitle());
            Container container =  addItem(article);

              this.form.add(container);
            //
        }
        Label title=new Label();
        Label content=new Label();
        Date created = new Date();
        Date updated = new Date();
        Button b2= new Button("partage fb");
        Button b3= new Button("stat");

        //form.add(b2);
        form.add(b3);

//        b2.addActionListener(e->{
//           Article pub=new Article(title.getText(),content
//                   .getText(),created,updated, "");
//
//            String token = "EAACEdEose0cBABlhXdgYMTbLgdXHZCSCnddvTLSKXXNYQRnaDhXxjZCqXZAPnhRUETihugC2ZBXY9GGBZCLoFwSOW7fuZBbVuZAoWnHmcZCSqDqjzQZAfzzlXmZBHF8TUEjRZBbPdbulrERbb3iXlKWjqByZA76rvfMrpzpXecyU30HZAEmZAcRfI42JEzt3dE40QkzxMZD";
//            FacebookClient fb = new DefaultFacebookClient(token);
//            FacebookType r = fb.publish("me/feed", FacebookType.class, Parameter.with("message", pub.toString()));
//            System.out.println("fb.com" + r.getId());
//
//        });

        b3.addActionListener(e->{
            Affichagestat pr = new Affichagestat();
            pr.getF().show();

        });
    }


    public Container addItem(Article article) {
        Container container = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        container.add(new SpanLabel("titre: "+article.getTitle()));
        SpanLabel c =new SpanLabel();
        c.setText("Contenue: "+ article.getContent());

        container.add(c);

        EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(300,300), true);
        URLImage background = URLImage.createToStorage(placeholder,article.getImage(),
                "http://localhost/"+article.getImage());
        background.fetch();

        ImageViewer img1 = new ImageViewer();
        img1.setImage(background);
        container.add(img1);


        return container;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }
}
