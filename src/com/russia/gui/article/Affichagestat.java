package com.russia.gui.article;

import com.codename1.charts.ChartComponent;
import com.codename1.charts.models.CategorySeries;
import com.codename1.charts.renderers.DefaultRenderer;
import com.codename1.charts.renderers.SimpleSeriesRenderer;
import com.codename1.charts.util.ColorUtil;
import com.codename1.charts.views.PieChart;
import com.codename1.components.ImageViewer;
import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.russia.entities.article.Article;
import com.russia.services.StatServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Affichagestat {

    Form f;
    TextField l;
    Form ListeArticle;
    ArrayList<Article> liste;
    Image img;
    ImageViewer image;
    Style s = UIManager.getInstance().getComponentStyle("M");
    FontImage p = FontImage.createMaterial(FontImage.MATERIAL_PORTRAIT, s);
    EncodedImage placeholder = EncodedImage.createFromImage(p.scaled(50,50), false);
    private EncodedImage enc;













    public  Affichagestat()
    {

        Map<String,Integer> listProduits = new HashMap<>();
        StatServices s=new StatServices();
        listProduits = s.getListNote();

        // Set up the renderer
        int[] colors = new int[]{ColorUtil.BLUE, ColorUtil.GREEN, ColorUtil.MAGENTA, ColorUtil.YELLOW, ColorUtil.CYAN,
                ColorUtil.BLACK, ColorUtil.GRAY, ColorUtil.LTGRAY, ColorUtil.WHITE};
        DefaultRenderer renderer = buildCategoryRenderer(colors);
        renderer.setZoomButtonsVisible(true);
        renderer.setZoomEnabled(true);
        renderer.setChartTitleTextSize(20);
        renderer.setDisplayValues(true);
        renderer.setShowLabels(true);

        SimpleSeriesRenderer r = renderer.getSeriesRendererAt(0);
        r.setGradientEnabled(true);
        r.setGradientStart(0, ColorUtil.BLUE);
        r.setGradientStop(0, ColorUtil.GREEN);
        r.setHighlighted(true);

        // Create the chart ... pass the values and renderer to the chart object.
        PieChart chart = new PieChart(buildCategoryDataset("Project budget", listProduits), renderer);

        // Wrap the chart in a Component so we can add it to a form
        ChartComponent c = new ChartComponent(chart);

        // Create a form and show it.
        Form f = new Form("badge/title");
        f.setLayout(new BorderLayout());
        f.addComponent(BorderLayout.CENTER, c);
        f.show();
        /*f.getToolbar().addCommandToRightBar("back", null, (ev) -> {
            HomeForm h = new HomeForm();
            h.getF().show();
        });*/

    }


    private DefaultRenderer buildCategoryRenderer(int[] colors) {
        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsTextSize(15);
        renderer.setLabelsColor(ColorUtil.BLACK);
        renderer.setLegendTextSize(15);
        renderer.setMargins(new int[]{20, 30, 15, 0});
        for (int color : colors) {
            SimpleSeriesRenderer r = new SimpleSeriesRenderer();
            r.setColor(color);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }


    protected CategorySeries buildCategoryDataset(String title, Map<String,Integer> values) {
        CategorySeries series = new CategorySeries(title);
        int k = 0;
        for(Map.Entry<String, Integer> entry : values.entrySet())

        {
            try {
                series.add("" + entry.getKey(), entry.getValue());
            } catch (ArithmeticException e) {
            }

        }

        return series;
    }


    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }






}