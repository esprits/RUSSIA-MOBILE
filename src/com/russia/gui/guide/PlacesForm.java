package com.russia.gui.guide;


import com.codename1.ui.*;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.guide.Place;
import com.russia.entities.guide.Region;
import com.russia.view.PlaceContainer;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.codename1.charts.util.ColorUtil.rgb;

public class PlacesForm {
    Form form;
    private Resources theme;
    private Region region;
    private Form regionForm;
    List<Place> filteredListState;
    List<Place> filteredList;


    public PlacesForm(Region region, Form regionForm) {
        this.region = region;
        this.regionForm = regionForm;
        theme = UIManager.initFirstTheme("/theme");
        this.form = new Form(region.getName().toUpperCase(), new BoxLayout(BoxLayout.Y_AXIS));
        form.getStyle().setBgColor(rgb(245, 245, 245));
        createPlaces(region.getPlaces());
        initToolbar();
    }

    private void initToolbar() {
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, s);
        form.getToolbar().addCommandToLeftBar("", icon, evt -> {
            regionForm.showBack();
        });

        addToolbarOverflow("Favourite", evt -> {
            PlaceFavouriteForm placeFavouriteForm = new PlaceFavouriteForm();
            placeFavouriteForm.getForm().show();
        });
        addToolbarOverflow("Filter", evt -> {
            Command[] cmds;
            Command okCommand = new Command("Submit");
            cmds = new Command[]{new Command("Canel"), okCommand};

            Container container = createDialog();
            List<String> categories = region.getPlaces().stream().map(place -> {
                return place.getCategory().getCode();
            }).distinct().collect(Collectors.toList());

            ButtonGroup bg = new ButtonGroup();

            RadioButton rbt = new RadioButton();
            rbt.setName("all");
            rbt.setText("All");
            bg.add(rbt);
            container.add(rbt);
            categories.forEach(cat -> {
                RadioButton radio = new RadioButton();
                radio.setName(cat);
                radio.setText(cat);
                bg.add(radio);
                container.add(radio);
            });
            if (Dialog.show("Choose Filter", container, cmds) == okCommand) {

                //categories.get(bg.getSelectedIndex()-1)
                if (bg.getSelectedIndex() != 0) {
                    form.removeAll();

                    filteredList = region.getPlaces().stream().filter(place -> place.getCategory().getCode()
                            .equals(categories.get(bg.getSelectedIndex() - 1))).collect(Collectors.toList());
                    createPlaces(filteredList);
                } else {
                    createPlaces(region.getPlaces());
                }
            }
        });
        addToolbarOverflow("Limit", evt -> {
            Command[] cmds;
            Command okCommand = new Command("Submit");
            cmds = new Command[]{new Command("Canel"), okCommand};

            Container container = createDialog();

            List<Integer> limits = Arrays.asList(5, 10, 25, 100);

            ButtonGroup bg = new ButtonGroup();

            limits.forEach(limit -> {
                RadioButton radio = new RadioButton();
                radio.setName(limit + "");
                radio.setText(limit + "");
                bg.add(radio);
                container.add(radio);
            });
            if (Dialog.show("Choose Limit", container, cmds) == okCommand) {
                form.removeAll();
                filteredListState = region.getPlaces().stream().limit(limits.get(bg.getSelectedIndex())).collect(Collectors.toList());
                createPlaces(filteredListState);
            }
        });
    }

    private Container createDialog() {
        Container dialogContainer = new Container(BoxLayout.y());
        return dialogContainer;
    }

    public void addToolbarOverflow(String name, ActionListener event) {
        form.getToolbar().addCommandToOverflowMenu(name, Resources.getGlobalResources().getImage(""), event);
    }

    private void createPlaces(List<Place> places) {
        places.forEach(place -> {
            form.add(new PlaceContainer(place));
        });
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }


}
