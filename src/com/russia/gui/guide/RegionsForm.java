package com.russia.gui.guide;

import com.codename1.charts.util.ColorUtil;
import com.codename1.components.SpanLabel;
import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.RoundBorder;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.guide.Place;
import com.russia.entities.guide.Region;
import com.russia.gui.HomeForm;
import com.russia.utils.PrefsUtils;
import com.russia.utils.ServiceUtils;
import com.russia.view.FavouriteContainer;
import com.russia.view.RegionContainer;

import java.util.Hashtable;
import java.util.List;

import static com.codename1.charts.util.ColorUtil.rgb;

public class RegionsForm {

    private static int total = 0;

    Form form;
    private Resources theme;


    public RegionsForm() {
        theme = UIManager.initFirstTheme("/theme");
        this.form = new Form("Explore RUSSIA", new BoxLayout(BoxLayout.Y_AXIS));
        form.getStyle().setBgColor(rgb(245, 245, 245));

        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, s);
        form.getToolbar().addCommandToLeftBar("", icon, evt -> {
            HomeForm homeForm = new HomeForm();
            homeForm.getForm().show();
        });
        form.getStyle().setBgTransparency(255);
        createPage();
        createRegions();
        createFavourite();
    }

    private void createPage() {
        Container container = new Container(BorderLayout.center());

        SpanLabel spanLabel = new SpanLabel("Welcome choose a City you want to visit".toUpperCase());
        Stroke borderStroke = new Stroke(2, Stroke.CAP_SQUARE, Stroke.JOIN_MITER, 1);
        spanLabel.getAllStyles().setPadding(50, 50, 0, 0);
        spanLabel.getAllStyles().setMargin(50, 50, 0, 0);
        spanLabel.getAllStyles().setBgTransparency(255);
        spanLabel.getAllStyles().setFgColor(rgb(255, 255, 255));
        spanLabel.getAllStyles().setBorder(RoundBorder.create().
                rectangle(true).
                color(rgb(240, 2, 2)).
                strokeColor(0).
                strokeOpacity(120).
                stroke(borderStroke));

//        spanLabel.getUnselectedStyle().setFont(Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_ITALIC, 24));

        container.add(BorderLayout.CENTER, spanLabel);
        form.add(container);
    }

    private void createRegions() {

        List<Region> regions = ServiceUtils.getRegions();
        Container container = new Container(BoxLayout.x());
        container.setScrollableX(true);
        container.setScrollableY(false);
        container.getStyle().setBgColor(ColorUtil.WHITE);
        container.getStyle().setBgTransparency(255);
        container.getStyle().setPadding(50, 50, 50, 50);
        container.getStyle().setMargin(20, 20, 20, 20);
        regions.forEach(region -> {
            container.add(new RegionContainer(region));
        });

        form.add(container);


    }

    public void createFavourite() {
        Container container = new Container(BoxLayout.x());
        container.setScrollableX(true);
        container.setScrollableY(false);
        container.getStyle().setBgColor(ColorUtil.WHITE);
        container.getStyle().setBgTransparency(255);
        container.getStyle().setPadding(50, 50, 50, 50);

        Hashtable<String, Object> prefs = PrefsUtils.getAll();
        prefs.forEach((s, o) -> {
            if (String.valueOf(o).equals("" + PlaceItemForm.FAVOURITE)) {
                Place place = ServiceUtils.getPlace(Integer.parseInt(s));
                container.add(new FavouriteContainer(place));
                total = total + 1;
            }
        });


        container.getStyle().setMargin(20, 20, 20, 20);

        if (total == 0)
            container.add(new Label("There's no favourite places yet"));
        form.add(container);
    }


    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }


}
