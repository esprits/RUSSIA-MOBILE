package com.russia.gui.guide;

import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
//import com.codename1.googlemaps.MapContainer;
import com.codename1.maps.Coord;
import com.codename1.ui.*;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.guide.Place;
import com.russia.utils.LocationFormatter;
import com.russia.utils.PrefsUtils;

import static com.codename1.charts.util.ColorUtil.rgb;

public class PlaceItemForm {
    public static final int FAVOURITE = 1;
    public static final int NOT_FAVOURITE = 0;

    private final String GOOGLE_MAP_KEY = "AIzaSyCmhiaaV-93EgDSXP2qmJZJtMwpVvJMcrw";


    private Form form;
    private Resources theme;
    private Place place;
    private Form placeForm;
    private boolean isStared = false;
    Command command;

    public PlaceItemForm(Place place, Form placeForm) {
        this.place = place;
        this.placeForm = placeForm;
        theme = UIManager.initFirstTheme("/theme");
        this.form = new Form(place.getName().toUpperCase(), new BoxLayout(BoxLayout.Y_AXIS));
        form.getStyle().setBgColor(rgb(245, 245, 245));

        initToolbar();
        createPlace();
        createMap();
    }


    private void initToolbar() {
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, s);
        form.setTitle(trim(place.getName()));
        form.getToolbar().addCommandToLeftBar("", icon, evt -> {
            placeForm.showBack();
        });

        addToolbarOverflow("Favourite", evt -> {
            PlaceFavouriteForm placeFavouriteForm = new PlaceFavouriteForm();
            placeFavouriteForm.getForm().show();
        });

        FontImage love;

        isStared = hasStar();
        love = (FontImage) getStarIcon(isStared);

        FloatingActionButton fab = FloatingActionButton.createFAB(getStarChar(isStared));
        fab.addActionListener(e -> {
            isStared = !hasStar();

            fab.setIcon(getStarIcon(isStared));
            PrefsUtils.setValue(String.valueOf(place.getId()), (isStared) ? PlaceItemForm.FAVOURITE : PlaceItemForm.NOT_FAVOURITE);
        });
        fab.bindFabToContainer(form.getContentPane());
    }

    private boolean hasStar() {
        return PrefsUtils.getValue(String.valueOf(place.getId()), PlaceItemForm.NOT_FAVOURITE) == PlaceItemForm.FAVOURITE;

    }


    private FontImage getStarIcon(boolean isStared) {
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        return FontImage
                .createMaterial(getStarChar(isStared), s);

    }

    private char getStarChar(boolean isStared) {
        return (isStared) ? FontImage.MATERIAL_STAR : FontImage.MATERIAL_STAR_BORDER;

    }

    private String trim(String name) {
        int maxLength = 20;
        if (name.length() < maxLength) {
            return name;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name.substring(0, maxLength)).append(" ...");

        return stringBuilder.toString();
    }


    private void createPlace() {


        EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight() / 3, 0xffff0000), true);
        URLImage placeImage = URLImage.createToStorage(placeholder, place.getPreviewPicture(), place.getPreviewPicture());
        ImageViewer imageViewer = new ImageViewer(placeImage);
        form.add(imageViewer);
        System.out.println("hehe");
        upperHolder();
        lowerHolder();
    }

    private void upperHolder() {

        Container upperHolder = new Container(BoxLayout.y());

        containerStyle(upperHolder);

        Container nameContainer = new Container(BorderLayout.center());
        nameContainer.add(BorderLayout.CENTER, new Label(place.getName()));
        Container categoryContainer = new Container(BorderLayout.center());
        categoryContainer.add(BorderLayout.CENTER, new Label(place.getCategory().getName()));

        upperHolder.add(nameContainer);
        upperHolder.add(categoryContainer);

        form.add(upperHolder);
    }

    public void addToolbarOverflow(String name, ActionListener event) {
        form.getToolbar().addCommandToOverflowMenu(name, Resources.getGlobalResources().getImage(""), event);
    }

    private void lowerHolder() {

        Container lowerHolder = new Container(BoxLayout.y());

        containerStyle(lowerHolder);

        SpanLabel information = new SpanLabel(place.getInformation());
        Label phone = new Label(place.getPhone());
        phone.getAllStyles().setFgColor(rgb(169, 169, 169));
        information.getAllStyles().setFgColor(rgb(169, 169, 169));

        Font materialFont = FontImage.getMaterialDesignFont();
        int size = Display.getInstance().convertToPixels(6, true);
        materialFont = materialFont.derive(size, Font.STYLE_PLAIN);
        phone.setIcon(FontImage.createFixed(String.valueOf(FontImage.MATERIAL_SETTINGS_PHONE), materialFont, rgb(200, 2, 2), 100, 100));


        System.out.println(place.getId());
        lowerHolder.add(information);
        lowerHolder.add(phone);

        form.add(lowerHolder);
    }

    private void containerStyle(Container container) {
        container.getStyle().setBgColor(rgb(255, 255, 255));
        container.getStyle().setBgTransparency(255);
        container.getStyle().setMargin(20, 30, 20, 20);
        container.getStyle().setPadding(20, 30, 20, 20);
    }

    private void createMap() {
//        final MapContainer cnt = new MapContainer(GOOGLE_MAP_KEY);
//
//
//        Coord position = new Coord(LocationFormatter.parse(place.getLocation().getLatitude()), LocationFormatter.parse(place.getLocation().getLongitude()));
//        System.out.println(position);
//        cnt.setCameraPosition(position);
//        cnt.zoom(position, 9);
//
//        cnt.setPreferredSize(new Dimension(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight() / 3));
//
//
//        Style s = new Style();
//        s.setFgColor(0xff0000);
//        s.setBgTransparency(0);
//        FontImage markerImg = FontImage.createMaterial(FontImage.MATERIAL_PLACE, s, Display.getInstance().convertToPixels(3));
//
//
//        cnt.addMarker(EncodedImage.createFromImage(markerImg, false), position, place.getName(), place.getPreviewText(), evt -> {
//            System.out.println("Clicked");
//        });
//
//
//        Container root = LayeredLayout.encloseIn(
//                BorderLayout.center(cnt)
//        );
//
//        form.add(root);

    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

}
