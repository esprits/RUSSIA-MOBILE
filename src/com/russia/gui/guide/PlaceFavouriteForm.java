package com.russia.gui.guide;

import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.guide.Place;
import com.russia.gui.HomeForm;
import com.russia.utils.PrefsUtils;
import com.russia.utils.ServiceUtils;
import com.russia.view.PlaceContainer;

import java.util.Hashtable;

import static com.codename1.charts.util.ColorUtil.rgb;

public class PlaceFavouriteForm {

    private static int total = 0;
    Resources theme;
    Form form;

    public PlaceFavouriteForm() {
        theme = UIManager.initFirstTheme("/theme");
        this.form = new Form("FAVOURITE PLACES", new BoxLayout(BoxLayout.Y_AXIS));
        form.getStyle().setBgColor(rgb(245, 245, 245));


        initToolbar();
        createPlace();
    }

    public void createPlace() {
        Hashtable<String, Object> prefs = PrefsUtils.getAll();
        prefs.forEach((s, o) -> {
            if (String.valueOf(o).equals("" + PlaceItemForm.FAVOURITE)) {
                Place place = ServiceUtils.getPlace(Integer.parseInt(s));
                form.add(new PlaceContainer(place, true));
                total = total + 1;
            }
        });
        if(total == 0){
            form.add(new Label("There's no favourite places yet"));
        }
        System.out.println(total);
    }


    private void initToolbar() {
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_CLOSE, s);
        form.getToolbar().addCommandToLeftBar("", icon, evt -> {
            HomeForm homeForm = new HomeForm();
            homeForm.getForm().show();
        });
    }


    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

}
