package com.russia.gui;


import com.codename1.components.ImageViewer;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.gui.Group.GroupListForm;
import com.russia.gui.Group.GroupListForm1;
import com.russia.gui.Group.GroupListForm2;

import com.russia.gui.article.ArticleForm;
import com.russia.gui.forum.Home;
import com.russia.gui.guide.PlaceFavouriteForm;
import com.russia.gui.guide.RegionsForm;
import com.russia.gui.match.FavoriteForm;
import com.russia.gui.match.ScheduleForm;
import com.russia.gui.match.ScoreForm;
import com.russia.gui.team.TeamListForm;

import java.io.IOException;

public class HomeForm {
    Form form;
    Resources theme;

    public HomeForm() {

        ScheduleForm sf = new ScheduleForm();
        ScoreForm scoreForm = new ScoreForm();
        scoreForm.getForm().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());
        GroupListForm1 groupForm = new GroupListForm1();
        groupForm.getForm().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());
        GroupListForm groupListForm = new GroupListForm();
        groupListForm.getF2().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());

        GroupListForm2 groupListForm2 = new GroupListForm2();
        groupListForm2.getF2().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());

        TeamListForm teamListForm = new TeamListForm();
        teamListForm.getForm().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());


        form = sf.getForm();
        theme = UIManager.initFirstTheme("/theme");

        Image icon = theme.getImage("russia2018.png");
        Container topBar = BorderLayout.north(new ImageViewer(icon));
        form.getToolbar().addComponentToSideMenu(topBar);

        form.getToolbar().addMaterialCommandToOverflowMenu("Home",FontImage.MATERIAL_HOME,evt -> {});

        form.getToolbar().addMaterialCommandToSideMenu("Home", FontImage.MATERIAL_HOME, e -> {
        });

        form.getToolbar().addMaterialCommandToSideMenu("News", FontImage.MATERIAL_FAVORITE, e -> {
            ArticleForm articleForm = new ArticleForm();
            articleForm.getForm().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());
            articleForm.getForm().show();
        });
        form.getToolbar().addMaterialCommandToSideMenu("Schedule", FontImage.MATERIAL_EVENT_NOTE, e -> {
            ScheduleForm scheduleForm = new ScheduleForm();
            scheduleForm.getForm().show();
        });

        form.getToolbar().addMaterialCommandToSideMenu("My Game", FontImage.MATERIAL_FAVORITE, e -> {
            FavoriteForm favoriteForm = new FavoriteForm();
            favoriteForm.getForm().show();
        });
        form.getToolbar().addMaterialCommandToSideMenu("Score", FontImage.MATERIAL_EVENT_AVAILABLE, e -> {
            scoreForm.getForm().show();
        });

        form.getToolbar().addMaterialCommandToSideMenu("Teams", FontImage.MATERIAL_EVENT_AVAILABLE, e -> {
            teamListForm.getForm().show();
        });

        form.getToolbar().addMaterialCommandToSideMenu("Groups", FontImage.MATERIAL_EVENT_AVAILABLE, e -> {
            groupForm.getForm().show();
        });

        form.getToolbar().addMaterialCommandToSideMenu("Rating-PooL", FontImage.MATERIAL_EVENT_AVAILABLE, e -> {

            groupListForm.getF2().show();
        });

        form.getToolbar().addMaterialCommandToSideMenu("FAN GUIDE", FontImage.MATERIAL_PLACE, e -> {
            RegionsForm regionsForm = new RegionsForm();
            regionsForm.getForm().show();
        });

        form.getToolbar().addMaterialCommandToSideMenu("Favourite Places", FontImage.MATERIAL_STAR, e -> {
            PlaceFavouriteForm placeFavouriteForm = new PlaceFavouriteForm();
            placeFavouriteForm.getForm().show();
        });
        form.getToolbar().addMaterialCommandToSideMenu("Forum", FontImage.MATERIAL_STAR, e -> {
            Home home = null;
            try {
                home = new Home();
                home.getF().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());
                home.getF().show();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        form.getToolbar().addMaterialCommandToSideMenu("About", FontImage.MATERIAL_INFO, e -> {
        });


    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }


}
