package com.russia.gui.forum;

import com.codename1.components.ImageViewer;
import com.codename1.ui.*;
import com.russia.entities.forum.subject;
import com.russia.services.ServiceSubject;

import java.io.IOException;

public class newSubject {
    private Form f;
    private TextField title,content;
    private Button btnajout, btnback, upload;

    private String fileNameInServer;


    public newSubject() {


        f = new Form("Propose subject");
        title = new TextField("", "Title");
        content = new TextField("", "Content",200, TextArea.ANY);
        upload = new Button("Upload");
        btnajout = new Button("Add");
        btnback = new Button("Cancel");
        f.add(title);
        f.add(content);
       // f.add(upload);
        f.add(btnajout);
        f.add(btnback);


        btnajout.addActionListener((e) -> {
            ServiceSubject service = new ServiceSubject();
            subject s;
            if("".equals(title.getText())){
                btnajout.addActionListener((evt) -> Dialog.show("Input Required", "Title is essential in this field", "OK", null));
            }else if("".equals(content.getText())){
                btnajout.addActionListener((evt) -> Dialog.show("Input Required", "Content is essential in this field", "OK", null));
            }else{
                try {
                    s = new subject(null, title.getText(), content.getText(),"coupe.png","Waiting" ,null,null );
                    service.ajoutSubject(s);
                    Home h = new Home();
                    h.getF().show();
                } catch (IOException ex) { }
            }
        });

        btnback.addActionListener((e)->{
            try {
                Home h = new Home();
                h.getF().show();
            } catch (IOException ex) {}
        });
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
}
