package com.russia.gui.forum;


import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.*;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.russia.entities.forum.subject;
import com.russia.services.ServiceSubject;
import java.io.IOException;

public class Home {
    Form f;
    public static int idU = 31;
    public void addS (subject s) throws IOException{

        SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
        String date = formater.format(s.getCreated());
        Container c = new Container(BoxLayout.y());
        Label l1 = new Label(s.getTitle());
        try{
            EncodedImage image = EncodedImage.create("/giphy.gif");
            Image img1 = URLImage.createToStorage(image, s.getImage(),
                    "http://localhost:8000//RUSSIA-WEB//web//assets//images//Sujet//"+
                            s.getImage()).scaled(150, 150);
            ImageViewer imgv1 = new ImageViewer(img1);
            Button shows = new Button("Show");
            c.add(imgv1);
            c.add(l1);
            c.add(date);
            c.add(shows);
            f.add(c);
            shows.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(com.codename1.ui.events.ActionEvent actionEvent) {
                    try {
                        afficheSubject aff = new afficheSubject(s);
                    } catch (IOException ex) {}
                }
            });
        } catch (IOException ex) {
            ex.getMessage();
        }

//        l1.addPointerPressedListener (new ActionListener<>() {
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//                try {
//                    afficheSubject aff = new afficheSubject(s);
//                } catch (IOException ex) {}
//            }
//        });
    }

    public Home() throws IOException {

        f = new Form(BoxLayout.y());
        f.setScrollable(true);
        f.setTitle("Forum");
        ServiceSubject ss = new ServiceSubject();
        for (subject s : ss.getList2())
        {
            addS(s);
        }

        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        fab.addActionListener((e) ->   {
            newSubject n = new newSubject();
            n.getF().show();
        });
        fab.bindFabToContainer(f.getContentPane());



        f.getToolbar().addSearchCommand((com.codename1.ui.events.ActionEvent e) -> {
            String text = (String)e.getSource();
            if(text == null || text.length() == 0) {
                f.removeAll();
                for (subject s : ss.getList2())
                {
                    try {
                        addS(s);
                    } catch (IOException ex) {}
                }
                for(Component cmp : f.getContentPane()) {
                    cmp.setHidden(false);
                    cmp.setVisible(true);
                }
                f.getContentPane().animateLayout(150);
            } else {
                text = text.toLowerCase();
                f.removeAll();
                for (subject s : ss.findByname(text))
                {
                    try {
                        addS(s);
                    } catch (IOException ex) {}
                }
                f.getContentPane().animateLayout(150);
            }
        });


        f.show();
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
}
