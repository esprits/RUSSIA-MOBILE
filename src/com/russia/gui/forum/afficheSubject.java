package com.russia.gui.forum;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.*;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.russia.entities.forum.comment;
import com.russia.entities.forum.subject;
import com.russia.services.ServiceComment;

import java.io.IOException;

public class afficheSubject {
    Form f2;
    SpanLabel lb;
    comment comments;
    Container c2;
    public static int currentSubject = 0;


    public void addC (comment c) throws IOException{
        SimpleDateFormat formater = new SimpleDateFormat("HH:mm dd-MM-yyyy ");
        String date = formater.format(c.getCreated());
        Container container = new Container(BoxLayout.y());
        Container dd = new Container(BoxLayout.x());
        String createdC = formater.format(c.getCreated());
        SpanLabel create = new SpanLabel(createdC);
        SpanLabel ContentC = new SpanLabel(c.getContent());
        EncodedImage image = EncodedImage.create("/giphy.gif");
        Image img1 = URLImage.createToStorage(image,"user.png" ,
                "http://localhost:8000//RUSSIA-WEB//web//assets//images//user.png").scaled(50, 50);
        ImageViewer imgv = new ImageViewer(img1);

        container.add(create);
        container.add(ContentC);
        dd.add(imgv);
        dd.add(container);
        c2.add(new Label("--------------------------------------------------------------"));
        c2.add(dd);
    }


    public afficheSubject(subject s) throws IOException {
        currentSubject = s.getId();
        f2 = new Form(s.getTitle(),new BorderLayout());
        ///////// container c1 ///////////////
        Container c1 = new Container(BoxLayout.y());
        SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
        String created = formater.format(s.getCreated());
        SpanLabel Content = new SpanLabel(s.getContent());
        EncodedImage image = EncodedImage.create("/giphy.gif");
        Image img1 = URLImage.createToStorage(image, s.getImage(),
                "http://localhost:8000//RUSSIA-WEB//web//assets//images//Sujet//"+
                        s.getImage()).scaled(150, 150);
        ImageViewer imgv1 = new ImageViewer(img1);
        c1.add(imgv1);
        //c1.add(created);
        c1.add(Content);
        ///////// container c2 ////////////////
        c2 = new Container(BoxLayout.y());
        ServiceComment sc = new ServiceComment();
        //f2.add(c2);
        Label comment;
        int taille = sc.getList2Comments(s).size();
        if(taille == 0){
            comment = new Label("No comments");
        }else if(taille == 1){
            comment = new Label("One comment") ;
        }else{
            comment = new Label(taille + " comments");
        }
        c2.add(comment);
        for(comment cc : sc.getList2Comments(s)){
            addC(cc);
        }

        ////////////// container c3 ////////////////
        Container c3 = new Container(BoxLayout.y());
        TextField setComment = new TextField("","Write a comment...");
        Button Done = new Button("send");
        c3.add(setComment);
        c3.add(Done);
        Container c1c2 = new Container(BoxLayout.y());
        c1c2.setScrollableY(true);
        c1c2.add(c1);
        c1c2.add(c2);
        ////////////// Back Button ////////////////////
        f2.getToolbar().addCommandToLeftBar("Back",null ,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    Home h=new Home();
                    h.getF().show();
                } catch (IOException ex) {}
            }
        });
//        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_UNDO);
//        fab.addActionListener((e) ->   {
//            try {
//                Home h=new Home();
//                h.getF().show();
//            } catch (IOException ex) {}
//        });
//        fab.bindFabToContainer(f2.getContentPane());

        ////////////// set comment Button ////////////////////
        Done.addActionListener((send)->{
            try {
                comments = new comment(null, setComment.getText(), null, null, s);
                sc.ajoutComment(comments);
                addC(comments);
//                afficheSubject a = new afficheSubject(s);
//                a.getF().show();
            } catch (IOException ex) {}
        });
        //////////////////////////////////////////////
        f2.add(BorderLayout.CENTER , c1c2).add(BorderLayout.SOUTH, c3);
        f2.setScrollableY(true);
        f2.show();
    }

    public Form getF() {
        return f2;
    }

    public void setF(Form f2) {
        this.f2 = f2;
    }
}
