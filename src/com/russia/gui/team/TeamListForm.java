package com.russia.gui.team;

import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.team.Team;
import com.russia.services.TeamService;

import javax.swing.*;

import java.io.IOException;

import static com.codename1.charts.util.ColorUtil.rgb;

public class TeamListForm {
    Form form;
    private Resources theme;

    public TeamListForm()  {
        theme = UIManager.initFirstTheme("/theme");
        this.form = new Form("Teams", BoxLayout.y());
        form.getStyle().setBgColor(rgb(245, 245, 245));
        TeamService teamService = new TeamService();
        for (Team team : teamService.getAll()) {
            form.add(addItem(team));
        }
    }

    private Container addItem(Team team) {
        Container components = new Container(BoxLayout.x());

        Container teamContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        teamContainer.getStyle().setMargin(30, 30, 50, 30);
        teamContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        teamContainer.getStyle().setMargin(30, 30, 30, 50);
        teamContainer.getStyle().setPadding(30, 30, 30, 30);

        Image resizedImage = theme.getImage(team.getTeamLogo()).scaledWidth(Math.round(Display.getInstance().getDisplayWidth() / 10)); //change value as necessary
        teamContainer.add(resizedImage);
        Label label = new Label(team.getTeamName().toUpperCase());

        components.getStyle().setBgColor(rgb(255, 255, 255));
        components.getStyle().setBgTransparency(255);
        components.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        components.getStyle().setMargin(10, 10, 20, 20);

        components.add(teamContainer);
        components.add(label);

        components.setLeadComponent(label);

        TeamDisplayForm teamDisplayForm = new TeamDisplayForm(team);
        teamDisplayForm.getForm().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());

        label.addPointerPressedListener(actionEvent ->
            teamDisplayForm.getForm().show()
        );

        return components;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }
}
