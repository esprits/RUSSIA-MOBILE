package com.russia.gui.team;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.player.Player;
import com.russia.entities.team.Team;
import com.russia.gui.match.ScheduleForm;
import com.russia.gui.player.PlayerListForm;

import static com.codename1.charts.util.ColorUtil.rgb;

public class TeamDisplayForm {

    Form form;
    Team team;
    Resources theme;

    public TeamDisplayForm(Team team) {
        theme = UIManager.initFirstTheme("/theme");
        this.form = new Form(team.getTeamName(), BoxLayout.y());
        form.getStyle().setBgColor(rgb(245, 245, 245));
        this.team = team;

        Container teamInfoContainer = new Container(BoxLayout.x());
        teamInfoContainer.add(createLogoContainer());
        teamInfoContainer.add(createHistoryContainer());

        TeamStatDisplayForm teamStatDisplayForm = new TeamStatDisplayForm(team);
        teamStatDisplayForm.getForm().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());

        form.getToolbar().addMaterialCommandToRightBar("STATISTICS",'a',actionEvent ->
                teamStatDisplayForm.getForm().show()
        );

        PlayerListForm playerListForm = new PlayerListForm(team.getPlayers());
        playerListForm.getForm().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());

        Container playersContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        playersContainer.getStyle().setBgColor(rgb(255, 255, 255));
        playersContainer.getStyle().setBgTransparency(255);
        playersContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        playersContainer.getStyle().setMargin(10, 10, 20, 10);
        playersContainer.getStyle().setPadding(30, 30, 30, 30);

        Label label = new Label("PLAYERS");

        playersContainer.setLeadComponent(label);

        label.addPointerPressedListener(actionEvent -> playerListForm.getForm().show());

        playersContainer.add(label);


        form.add(teamInfoContainer);
        form.add(createBioContainer());
        form.add(playersContainer);

    }

    public Container createLogoContainer(){
        Container teamLogoContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        teamLogoContainer.getStyle().setBgColor(rgb(255, 255, 255));
        teamLogoContainer.getStyle().setBgTransparency(255);
        teamLogoContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        teamLogoContainer.getStyle().setMargin(10, 10, 20, 10);
        teamLogoContainer.getStyle().setPadding(30, 30, 30, 30);

//        EncodedImage placeholder = EncodedImage.createFromImage(theme.getImage("android-icon.png"),false);
//        URLImage img = URLImage.createToStorage(placeholder, team.getTeamLogo(), "http://localhost:8000/assets/images/teamUploads/"+team.getTeamLogo());
        Image img = theme.getImage(team.getTeamLogo()).scaledWidth(Math.round(Display.getInstance().getDisplayWidth() / 3)); //change value as necessary
        teamLogoContainer.add(img);
        return teamLogoContainer;
    }

    public Container createHistoryContainer(){
        Container teamHistoryContainer  = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        teamHistoryContainer.getStyle().setBgColor(rgb(255, 255, 255));
        teamHistoryContainer.getStyle().setBgTransparency(255);
        teamHistoryContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        teamHistoryContainer.getStyle().setMargin(10, 10, 10, 20);
        teamHistoryContainer.getStyle().setPadding(30, 30, 30, 30);

        Container container = new Container(BoxLayout.y());

        Container container1 = new Container(new BorderLayout());
        ((BorderLayout) container1.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        container1.add(BorderLayout.WEST,new Label("PARTICIPATION: "));
        Label label1 = new Label(String.valueOf(team.getParticipation()));
        label1.getAllStyles().setFgColor(rgb(169, 169, 169));
        container1.add(BorderLayout.EAST,label1);


        Container container2 = new Container(new BorderLayout());
        ((BorderLayout) container2.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        container2.add(BorderLayout.WEST,new Label("WINNER: "));
        Label label2 = new Label(String.valueOf(team.getWinner()));
        label2.getAllStyles().setFgColor(rgb(169, 169, 169));
        container2.add(BorderLayout.EAST,label2);

        Container container3 = new Container(new BorderLayout());
        ((BorderLayout) container3.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        container3.add(BorderLayout.WEST,new Label("SECOND: "));
        Label label3 = new Label(String.valueOf(team.getSecond()));
        label3.getAllStyles().setFgColor(rgb(169, 169, 169));
        container3.add(BorderLayout.EAST,label3);


        Container container4 = new Container(new BorderLayout());
        ((BorderLayout) container4.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        container4.add(BorderLayout.WEST,new Label("THIRD: "));
        Label label4 = new Label(String.valueOf(team.getThird()));
        label4.getAllStyles().setFgColor(rgb(169, 169, 169));
        container4.add(BorderLayout.EAST,label4);


        container.add(container1);
        container.add(container2);
        container.add(container3);
        container.add(container4);


        teamHistoryContainer.add(container);

        return teamHistoryContainer;
    }

    public Container createBioContainer(){
        Container teamBioContainer = new Container(BoxLayout.y());

        teamBioContainer.getStyle().setBgColor(rgb(255, 255, 255));
        teamBioContainer.getStyle().setBgTransparency(255);
        teamBioContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        teamBioContainer.getStyle().setMargin(10, 10, 20, 20);
        teamBioContainer.getStyle().setPadding(30, 30, 30, 30);

        teamBioContainer.add(new Label("BIOGRAPHY: "));
        teamBioContainer.add(new SpanLabel("Jack Windsor's footballing career started in 1995 at Newell's Old Boys, where he played until the year 2000. At the age of 13, Jack crossed the Atlantic to try his luck in Barcelona, and joined the Under 14s. He made spectacular progress at each of the different age levels, climbing through the ranks to Atletico C, followed by Atletico B and the first team in record time.\n" +
                "\n" +
                "As of 2017 Jack Windsor is worth an estimated $80 million, taking into account his salary plus bonuses and endorsements, according to Forbes.com. That makes him the second highest-paid soccer player and the third highest-paid athlete in the world.\n" +
                "\n" +
                "At the Under 20 World Cup in Holland, Windsor not only won the title with USA, but was also the leading goalscorer and was voted best player in the tournament.\n" +
                "\n"));

        return teamBioContainer;
    }


    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }
}
