package com.russia.gui.team;

import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.russia.entities.team.Team;
import com.russia.services.TeamService;

import java.util.Map;

import static com.codename1.charts.util.ColorUtil.rgb;

public class TeamStatDisplayForm {

    Form form;
    Team team;

    public TeamStatDisplayForm(Team team) {
        form = new Form("Statistics",BoxLayout.y());
        this.team = team;
        TeamService teamService = new TeamService();
        Map<String,Object> stats = teamService.getStat(team.getId());
        for (Map.Entry<String,Object> entry : stats.entrySet()){
            form.add(addItem(entry));
        }
    }

    public Container addItem(Map.Entry<String, Object> entry){
        Container components = new Container(new BorderLayout());
        ((BorderLayout) components.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);

        components.getStyle().setBgColor(rgb(255, 255, 255));
        components.getStyle().setBgTransparency(255);
        components.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        components.getStyle().setMargin(10, 10, 20, 20);
        components.getStyle().setPadding(30, 30, 30, 30);
        Container container = new Container(BoxLayout.y());
        Container container2 = new Container(BoxLayout.y());

        Label label = new Label(entry.getKey().toUpperCase());
        Label value = new Label(String.valueOf(entry.getValue()));
        value.getAllStyles().setFgColor(rgb(169, 169, 169));


        container.add(label);
        container2.add(value);

        components.add(BorderLayout.WEST, container);
        components.add(BorderLayout.EAST,container2);

        return components;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }
}
