package com.russia.gui.payment;

import com.codename1.components.ToastBar;
import com.codename1.ui.*;
import com.codename1.ui.events.DataChangedListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.NumericConstraint;
import com.codename1.ui.validation.Validator;
import com.russia.entities.match.Match;
import com.russia.entities.ticket.Ticket;
import com.russia.gui.HomeForm;
import com.russia.payment.CreditCard;
import com.russia.payment.StripeClient;
import com.russia.utils.ServiceUtils;

import java.io.IOException;

import static com.codename1.charts.util.ColorUtil.rgb;

public class PaymentForm {
    private Form form;
    private Resources theme;
    private static final char space = ' ';
    private Match match;


    public PaymentForm(Match match, Form forms) {
        this.match = match;

        Ticket ticket = ServiceUtils.getTicket(match.getId());
        ticket.setMatch(match);

        theme = UIManager.initFirstTheme("/theme");
        this.form = new Form("Checkout", new BoxLayout(BoxLayout.Y_AXIS));
        form.getStyle().setBgColor(rgb(245, 245, 245));
        TextField firstName = new TextField("", "Card Holder Name", 20, TextArea.ANY);

        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, s);
        form.getToolbar().addCommandToLeftBar("", icon, evt -> {
            forms.showBack();

        });

        // Credit Card
        Container creditCardContainer = new Container(new GridLayout(1, 4));
        final TextField num1 = new TextField(4);
        final TextField num2 = new TextField(4);
        final TextField num3 = new TextField(4);
        final TextField num4 = new TextField(4);
        num1.setConstraint(TextArea.NUMERIC);
        num2.setConstraint(TextArea.NUMERIC);
        num3.setConstraint(TextArea.NUMERIC);
        num4.setConstraint(TextArea.NUMERIC);
        creditCardContainer.addComponent(num1);
        creditCardContainer.addComponent(num2);
        creditCardContainer.addComponent(num3);
        creditCardContainer.addComponent(num4);
        // ####


        // Date
        Container dateContainer = new Container(new GridLayout(1, 3));
        final TextField month = new TextField(2);
        final TextField year = new TextField(2);
        month.setConstraint(TextArea.NUMERIC);
        year.setConstraint(TextArea.NUMERIC);
        month.setHint("mm");
        year.setHint("yy");
        dateContainer.addComponent(month);
        dateContainer.addComponent(new Label("/"));
        dateContainer.addComponent(year);
        // ##########



        TextField zipCode = new TextField("", "ZIP CODE", 20, TextArea.NUMERIC);
        TextField cvv = new TextField("", "CVV", 20, TextArea.NUMERIC);


        form.addComponent(creditCardContainer);
        form.add(dateContainer);
        form.add(firstName);

        Validator v = new Validator();
        v.addConstraint(num1, new LengthConstraint(4)).
                addConstraint(num2, new LengthConstraint(4)).
                addConstraint(num2, new NumericConstraint(false)).
                addConstraint(num3, new NumericConstraint(false)).
                addConstraint(num4, new NumericConstraint(false)).
                addConstraint(num1, new NumericConstraint(false)).
                addConstraint(num3, new LengthConstraint(4)).
                addConstraint(num4, new LengthConstraint(4)).
                addConstraint(month, new LengthConstraint(2)).
                addConstraint(year, new LengthConstraint(2))
                .addConstraint(month, new NumericConstraint(false, 0, 12, "Wrong month"))
                .addConstraint(year, new NumericConstraint(false, 18, 99, "Wrong year"));

        automoveToNext(num1, num2);
        automoveToNext(num2, num3);
        automoveToNext(num3, num4);
        automoveToNextDate(month, year);
        automoveToNextDate(year, firstName);


        Button submit = new Button("Submit");
        form.add(submit);
        v.addSubmitButtons(submit);
        submit.addActionListener(evt -> {
            String creditCard = num1.getText() + num2.getText() + num3.getText() + num4.getText();
            CreditCard creditCard1 = new CreditCard();
            creditCard1.setCardNumber(creditCard);

            pay(ticket);
        });
    }

    private void createCreditCard() {

    }

    private void automoveToNext(final TextField current, final TextField next) {
        current.addDataChangeListener(new DataChangedListener() {
            public void dataChanged(int type, int index) {
                if (current.getText().length() == 5) {
                    Display.getInstance().stopEditing(current);
                    String val = current.getText();
                    current.setText(val.substring(0, 4));
                    next.setText(val.substring(4));
                    Display.getInstance().editString(next, 5, current.getConstraint(), next.getText());
                }
            }
        });
    }

    private void automoveToNextDate(final TextField current, final TextField next) {
        current.addDataChangeListener(new DataChangedListener() {
            public void dataChanged(int type, int index) {
                if (current.getText().length() == 3) {
                    Display.getInstance().stopEditing(current);
                    String val = current.getText();
                    current.setText(val.substring(0, 2));
                    next.setText(val.substring(2));
                    Display.getInstance().editString(next, 3, current.getConstraint(), next.getText());
                }
            }
        });
    }

    private void pay(Ticket ticket) {
        StripeClient stripeClient = new StripeClient();
        try {
            stripeClient.charge(ticket.getPrice() * 100, match.getFirstTeam().getTeamName() + " VS " + match.getSecondTeam().getTeamName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Form getForm() {
        return form;
    }

    public PaymentForm setForm(Form form) {
        this.form = form;
        return this;
    }
}
