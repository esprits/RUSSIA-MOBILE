package com.russia.gui.player;

import com.codename1.ui.*;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.player.Player;
import com.russia.services.PlayerService;


import java.util.ArrayList;
import java.util.List;

import static com.codename1.charts.util.ColorUtil.rgb;

public class PlayerListForm {

    Form form;
    Resources theme;
    List<Player> playerList;
    public PlayerListForm(List<Player> playerList) {
        theme = UIManager.initFirstTheme("/theme");
        form = new Form("Players", BoxLayout.y());
        this.playerList = playerList;
        form.getStyle().setBgColor(rgb(245, 245, 245));
        for (Player player : playerList) {
            form.add(addItem(player));
        }
    }

    private Container addItem(Player player) {

        Container components = new Container(BoxLayout.x());

        Container playerContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        playerContainer.getStyle().setMargin(30, 30, 50, 30);
        playerContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        playerContainer.getStyle().setMargin(30, 30, 30, 50);
        playerContainer.getStyle().setPadding(30, 30, 30, 30);

        Image resizedImage = theme.getImage(player.getPlayerImage()).scaledWidth(Math.round(Display.getInstance().getDisplayWidth() / 10)); //change value as necessary
        playerContainer.add(resizedImage);
        Label label = new Label(player.getPlayerLastName().toUpperCase()+" "+player.getPlayerName());

        components.getStyle().setBgColor(rgb(255, 255, 255));
        components.getStyle().setBgTransparency(255);
        components.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        components.getStyle().setMargin(10, 10, 20, 20);

        components.add(playerContainer);
        components.add(label);

        components.setLeadComponent(label);

        PlayerDisplayForm playerDisplayForm = new PlayerDisplayForm(player);
        playerDisplayForm.getForm().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());

        label.addPointerPressedListener(actionEvent ->
                playerDisplayForm.getForm().show()
        );

        return components;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }
}
