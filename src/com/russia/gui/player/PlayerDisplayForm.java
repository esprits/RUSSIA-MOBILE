package com.russia.gui.player;

import com.codename1.components.SpanLabel;
import com.codename1.ui.*;
import com.codename1.ui.geom.Rectangle;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.player.Club;
import com.russia.entities.player.Player;
import com.russia.entities.player.Skill;
import com.russia.entities.player.View;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.List;

import static com.codename1.charts.util.ColorUtil.rgb;

public class PlayerDisplayForm {

    Form form;
    Player player;
    Resources theme;

    public PlayerDisplayForm(Player player) {
        theme = UIManager.initFirstTheme("/theme");
        form = new Form(player.getPlayerName()+" "+player.getPlayerLastName(),BoxLayout.y());
        form.getStyle().setBgColor(rgb(245, 245, 245));
        this.player = player;

        Container playerInfoContainer = new Container(BoxLayout.x());
        playerInfoContainer.add(createLogoContainer());
        playerInfoContainer.add(createHistoryContainer());

        PlayerStatDisplayForm playerStatDisplayForm = new PlayerStatDisplayForm(player);
        playerStatDisplayForm.getForm().getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt -> form.showBack());

        form.getToolbar().addMaterialCommandToRightBar("STATISTICS",'a',actionEvent ->
                playerStatDisplayForm.getForm().show()
        );



        form.add(playerInfoContainer);
        form.add(createSkillsContainer());
        form.add(createBioContainer());
        form.add(createClubContainer());
        form.add(createViewContainer());


    }

    private Container createViewContainer() {

        Container playerViewsContainer  = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        playerViewsContainer.getStyle().setBgColor(rgb(255, 255, 255));
        playerViewsContainer.getStyle().setBgTransparency(255);
        playerViewsContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        playerViewsContainer.getStyle().setMargin(10, 10, 10, 20);
        playerViewsContainer.getStyle().setPadding(30, 30, 30, 30);

        if (player.getViews().size()==1)
            playerViewsContainer.add(new Label("One View"));
        else if (player.getViews().size()==0)
            playerViewsContainer.add(new Label("No Views Yet"));
        else
            playerViewsContainer.add(new Label(player.getViews().size()+" Views"));

        List<View> viewList = player.getViews();

        for (View view : viewList)
        {
            playerViewsContainer.add(addView(view));
        }
        return playerViewsContainer;
    }

    private Container addView(View view) {
        Container container = new Container(BoxLayout.y());

        container.getStyle().setBgColor(rgb(255, 255, 255));
        container.getStyle().setBgTransparency(255);
        container.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        container.getStyle().setMargin(10, 10, 10, 20);

        Container container1 = new Container();
        container1.getStyle().setBgColor(rgb(200, 200, 200));
        container1.getStyle().setBgTransparency(255);
        container1.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        container1.getStyle().setPadding(30, 30, 10, 30);

        Container container2 = new Container();
        container2.getStyle().setBgColor(rgb(255, 255, 255));
        container2.getStyle().setBgTransparency(255);
        container2.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        container2.getStyle().setPadding(30, 30, 30, 30);

        container1.add(new Label(view.getUser()));
        container2.add(new Label(view.getPost()));

        container.add(container1);
        container.add(container2);

        return container;
    }

    private Container createClubContainer() {
        Container playerClubsContainer  = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        playerClubsContainer.getStyle().setBgColor(rgb(255, 255, 255));
        playerClubsContainer.getStyle().setBgTransparency(255);
        playerClubsContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        playerClubsContainer.getStyle().setMargin(10, 10, 10, 20);
        playerClubsContainer.getStyle().setPadding(30, 30, 30, 30);

        List<Club> clubList = player.getClubs();
        clubList.sort(Comparator.comparingInt(Club::getSeasonStart));

        for (Club club : clubList)
        {
            playerClubsContainer.add(addClub(club));
        }
        return playerClubsContainer;
    }

    private Container addClub(Club club) {
        Container container = new Container(BoxLayout.y());

        container.getStyle().setBgColor(rgb(255, 255, 255));
        container.getStyle().setBgTransparency(255);
        container.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        container.getStyle().setMargin(10, 10, 10, 20);

        Container container1 = new Container();
        container1.getStyle().setBgColor(rgb(200, 200, 200));
        container1.getStyle().setBgTransparency(255);
        container1.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        container1.getStyle().setPadding(30, 30, 30, 30);

        container1.add(new Label(club.getSeasonStart() +" "+club.getClubName()));

        container.add(container1);
        container.add(new Label("GOALS SCORED: "+club.getGoalScored()));
        container.add(new Label("MATCH PLAYED: "+club.getMatchPlayed()));
        return container;
    }

    private Component createBioContainer() {
        Container playerBioContainer = new Container(BoxLayout.y());

        playerBioContainer.getStyle().setBgColor(rgb(255, 255, 255));
        playerBioContainer.getStyle().setBgTransparency(255);
        playerBioContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        playerBioContainer.getStyle().setMargin(10, 10, 20, 20);
        playerBioContainer.getStyle().setPadding(30, 30, 30, 30);

        playerBioContainer.add(new Label("BIOGRAPHY: "));
        playerBioContainer.add(new SpanLabel(player.getBio()));

        return playerBioContainer;
    }

    private Container createHistoryContainer() {
        Container playerHistoryContainer  = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        playerHistoryContainer.getStyle().setBgColor(rgb(255, 255, 255));
        playerHistoryContainer.getStyle().setBgTransparency(255);
        playerHistoryContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        playerHistoryContainer.getStyle().setMargin(10, 10, 10, 20);
        playerHistoryContainer.getStyle().setPadding(30, 30, 30, 30);

        Container container = new Container(BoxLayout.y());

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Container container1 = new Container(new BorderLayout());
        ((BorderLayout) container1.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        container1.add(BorderLayout.WEST,new Label("BIRTHDAY: "));
        Label label1 = new Label(df.format(player.getBirthday()));
        label1.getAllStyles().setFgColor(rgb(169, 169, 169));
        container1.add(BorderLayout.EAST,label1);

        Container container2 = new Container(new BorderLayout());
        ((BorderLayout) container2.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        container2.add(BorderLayout.WEST,new Label("TOTAL GAMES: "));
        Label label2 = new Label(String.valueOf(player.getTotalGames()));
        label2.getAllStyles().setFgColor(rgb(169, 169, 169));
        container2.add(BorderLayout.EAST,label2);


        Container container3 = new Container(new BorderLayout());
        ((BorderLayout) container3.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        container3.add(BorderLayout.WEST,new Label("HEIGHT: "));
        Label label3 = new Label(String.valueOf(player.getHeight()));
        label3.getAllStyles().setFgColor(rgb(169, 169, 169));
        container3.add(BorderLayout.EAST,label3);


        Container container4 = new Container(new BorderLayout());
        ((BorderLayout) container4.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        container4.add(BorderLayout.WEST,new Label("WEIGHT: "));
        Label label4 = new Label(String.valueOf(player.getWeight()));
        label4.getAllStyles().setFgColor(rgb(169, 169, 169));
        container4.add(BorderLayout.EAST,label4);


        container.add(container1);
        container.add(container2);
        container.add(container3);
        container.add(container4);


        playerHistoryContainer.add(container);

        return playerHistoryContainer;

    }

    private Container createSkillsContainer() {
        Container playerSkillsContainer  = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        playerSkillsContainer.getStyle().setBgColor(rgb(255, 255, 255));
        playerSkillsContainer.getStyle().setBgTransparency(255);
        playerSkillsContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        playerSkillsContainer.getStyle().setMargin(10, 10, 10, 20);
        playerSkillsContainer.getStyle().setPadding(30, 30, 30, 30);

        for (Skill skill : player.getSkills())
        {
            playerSkillsContainer.add(addSkill(skill));
        }

        return playerSkillsContainer;

    }

    private Container addSkill(Skill skill) {
        Container progress = new Container(new LayeredLayout());
        Label percent = new Label(skill.getLabel().toUpperCase()+": "+skill.getValue()+"%");
        percent.getUnselectedStyle().setAlignment(Component.CENTER);
        Image overLay = theme.getImage("round.png");
        progress.add(new Label(overLay, "Container")).
                add(FlowLayout.encloseCenterMiddle(percent));
        progress.getUnselectedStyle().setBgPainter((Graphics g, Rectangle rect) -> {
            g.setColor(0xff0000);
            g.fillArc(progress.getX(), progress.getY(), overLay.getWidth(), overLay.getHeight(), 90, skill.getValue()*-360/100);
        });
        return progress;
    }


    private Container createLogoContainer() {
        Container playerLogoContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        playerLogoContainer.getStyle().setBgColor(rgb(255, 255, 255));
        playerLogoContainer.getStyle().setBgTransparency(255);
        playerLogoContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        playerLogoContainer.getStyle().setMargin(10, 10, 20, 10);
        playerLogoContainer.getStyle().setPadding(30, 30, 30, 30);

        Image img = theme.getImage(player.getPlayerImage()).scaledWidth(Math.round(Display.getInstance().getDisplayWidth() / 3)); //change value as necessary
        playerLogoContainer.add(img);
        return playerLogoContainer;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }
}
