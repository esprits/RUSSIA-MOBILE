package com.russia.gui.player;


import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.russia.entities.player.Player;

import static com.codename1.charts.util.ColorUtil.rgb;

public class PlayerStatDisplayForm {

    Form form;
    Player player;

    public PlayerStatDisplayForm(Player player) {
        form = new Form("Statistics", BoxLayout.y());
        this.player = player;

        Container components = new Container(BoxLayout.y());

        components.add(createStat("GOALSCORED",String.valueOf(player.getGoalScored())));
        components.add(createStat("SHOTSONTARGET",String.valueOf(player.getShotsOnTarget())));
        components.add(createStat("SHOTS",String.valueOf(player.getShots())));
        components.add(createStat("ASSISTS",String.valueOf(player.getAssists())));
        components.add(createStat("FOULS",String.valueOf(player.getFouls())));
        components.add(createStat("CORNERKICKS",String.valueOf(player.getCornerKicks())));
        components.add(createStat("PENALTYKICKS",String.valueOf(player.getPenaltyKicks())));
        components.add(createStat("YELLOWCARDS",String.valueOf(player.getYellowCards())));
        components.add(createStat("REDCARDS",String.valueOf(player.getRedCards())));

        form.add(components) ;
    }

    public Container createStat(String label,String value){
        Container components = new Container(new BorderLayout());
        ((BorderLayout) components.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);

        components.getStyle().setBgColor(rgb(255, 255, 255));
        components.getStyle().setBgTransparency(255);
        components.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        components.getStyle().setMargin(10, 10, 20, 20);
        components.getStyle().setPadding(30, 30, 30, 30);

        Container container = new Container(BoxLayout.y());
        Container container2 = new Container(BoxLayout.y());

        Label label1 = new Label(label);
        Label value1 = new Label(String.valueOf(value));
        value1.getAllStyles().setFgColor(rgb(169, 169, 169));


        container.add(label1);
        container2.add(value1);

        components.add(BorderLayout.WEST, container);
        components.add(BorderLayout.EAST,container2);

        return components;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }
}
