package com.russia.gui.match;


import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.match.Score;
import com.russia.services.match.ScoreService;
import com.russia.utils.Constant;

import java.io.IOException;

import static com.codename1.charts.util.ColorUtil.rgb;

public class ScoreForm {
    Form form;
    private Resources theme;


    public ScoreForm(){
        theme = UIManager.initFirstTheme("/theme");
        form = new Form("Score",new BoxLayout(BoxLayout.Y_AXIS));
        form.getStyle().setBgColor(rgb(245,245,245));
        ScoreService scoreService = new ScoreService();
        for (Score score : scoreService.getListScore()) {
            try {
                form.add(addItem(score));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }


    private Container addItem(Score score) throws IOException {

        Container holder = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        holder.getStyle().setBgColor(rgb(255,255,255));
        holder.getStyle().setBgTransparency(255);
        holder.getStyle().setMargin(20,10,20,20);


        Container upperHolder = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Container stadiumContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        Label stadium  = new Label(score.getMatch().getStadium().toUpperCase());
        stadium.getAllStyles().setFgColor(rgb(169,169,169));
        stadiumContainer.add(BorderLayout.CENTER,stadium);

        Container levelContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        levelContainer.add(BorderLayout.CENTER,new Label(score.getMatch().getLevel().toUpperCase()));

        upperHolder.add(stadiumContainer);
        upperHolder.add(levelContainer);

        Container lowerHolder = new Container(new BorderLayout());
        ((BorderLayout)lowerHolder.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);

        Container firstTeamContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        firstTeamContainer.getStyle().setMargin(30, 30, 50, 30);
        firstTeamContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        firstTeamContainer.getStyle().setMargin(30, 30, 30, 50);
        firstTeamContainer.getStyle().setPadding(30, 30, 30, 30);


        EncodedImage imgHolder = EncodedImage.create("/giphy.gif");
        URLImage image =  URLImage.createToStorage(imgHolder, score.getMatch().getFirstTeam().getTeamLogo(),
                Constant.PATH+Constant.TEAM_DIR+score.getMatch().getFirstTeam().getTeamLogo());

        firstTeamContainer.add(BorderLayout.CENTER,image);
        Container shortCutContainer1 = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        shortCutContainer1.add(BorderLayout.CENTER,new Label(score.getMatch().getFirstTeam().getTeamShortcut().toUpperCase()));
        firstTeamContainer.add(BorderLayout.SOUTH,shortCutContainer1);
        Container centerContainer = new Container(new BorderLayout());
        ((BorderLayout)centerContainer.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);

        Label scoreT1 = new Label(String.valueOf(score.getScoreTeam1()));
        Label scoreT2 = new Label(String.valueOf(score.getScoreTeam2()));
        if (score.getScoreTeam1()>score.getScoreTeam2()){
            scoreT1.getAllStyles().setFgColor(0xff0000);
        }else if (score.getScoreTeam1()<score.getScoreTeam2()){
            scoreT2.getAllStyles().setFgColor(0xff0000);
        }
        centerContainer.add(BorderLayout.WEST,scoreT1);
        centerContainer.add(BorderLayout.CENTER,new Label(" -- "));
        centerContainer.add(BorderLayout.EAST,scoreT2);

        Button btnGameOverview = new Button("Overview");
        btnGameOverview.addActionListener(e->{
            GameOverviewForm gameOverviewForm = new GameOverviewForm(score.getMatch().getFirstTeam().getTeamName().toString().toUpperCase()+
                    " vs "+score.getMatch().getSecondTeam().getTeamName().toUpperCase(),score);
            gameOverviewForm.getForm().getToolbar().addMaterialCommandToLeftBar(null,FontImage.MATERIAL_ARROW_BACK,l->{
                this.form.showBack();
            });
            gameOverviewForm.getForm().show();
        });
        centerContainer.add(BorderLayout.SOUTH,btnGameOverview);

        centerContainer.getStyle().setBgTransparency(255);

        Container secondTeamContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        secondTeamContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        secondTeamContainer.getStyle().setMargin(30, 30, 30, 50);
        secondTeamContainer.getStyle().setPadding(30, 30, 30, 30);

        URLImage image1 =  URLImage.createToStorage(imgHolder, score.getMatch().getSecondTeam().getTeamLogo(),
                Constant.PATH+Constant.TEAM_DIR+score.getMatch().getSecondTeam().getTeamLogo());
        secondTeamContainer.add(BorderLayout.CENTER,image1);
        Container shortCutContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        shortCutContainer.add(BorderLayout.CENTER,new Label(score.getMatch().getSecondTeam().getTeamShortcut().toUpperCase()));
        secondTeamContainer.add(BorderLayout.SOUTH,shortCutContainer);


        lowerHolder.add(BorderLayout.WEST,firstTeamContainer);
        lowerHolder.add(BorderLayout.CENTER,centerContainer);
        lowerHolder.add(BorderLayout.EAST,secondTeamContainer);

        holder.add(upperHolder);
        holder.add(lowerHolder);

        return holder;
    }


}
