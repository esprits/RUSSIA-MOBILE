package com.russia.gui.match;

import com.codename1.components.ShareButton;
import com.codename1.components.ToastBar;
import com.codename1.io.FileSystemStorage;
import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.dao.MatchDAO;
import com.russia.entities.match.Match;
import com.russia.gui.HomeForm;
import com.russia.gui.payment.PaymentForm;
import com.russia.utils.Constant;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.codename1.charts.util.ColorUtil.rgb;

public class FavoriteForm {
    Form form;
    Resources theme;
    Label lblClose;

    public FavoriteForm() {
        theme = UIManager.initFirstTheme("/theme");
        form = new Form("My Game", new BoxLayout(BoxLayout.Y_AXIS));
        form.getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, et -> {
            HomeForm homeForm = new HomeForm();
            homeForm.getForm().show();

        });

        form.getStyle().setBgColor(rgb(245, 245, 245));
        MatchDAO matchDAO = new MatchDAO();
        ArrayList<Match> matchList = matchDAO.getFavoriteMatch();
        Collections.sort(matchList, new Comparator<Match>() {
            @Override
            public int compare(Match match1, Match match2)
            {
                return  match1.getDates().compareTo(match2.getDates());
            }
        });
        for (Match match : matchList) {
            try {
                form.add(addItem(match));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }


    private Container addItem(Match match) throws IOException {
        Container holder = new Container(BoxLayout.y());
        holder.getStyle().setBgColor(0xFFFFFF);
        holder.getStyle().setBgTransparency(255);
        holder.getStyle().setMargin(20, 10, 20, 20);

        Container upperHolder = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Container stadiumContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        Label stadium = new Label(match.getStadium().toUpperCase());
        stadium.getAllStyles().setFgColor(rgb(169, 169, 169));
        stadiumContainer.add(BorderLayout.CENTER, stadium);

        Image close = theme.getImage("ic_close.png");
        lblClose = new Label(close);

        lblClose.addPointerPressedListener(e -> {
            if (
                    Dialog.show(match.getFirstTeam().getTeamShortcut().toUpperCase() + " vs " + match.getSecondTeam().getTeamShortcut().toUpperCase(),
                            "Do you really want to remove this game from your favorite ?", "Yes", "Cancel")) {
                MatchDAO matchDAO = new MatchDAO();
                matchDAO.deleteMatch(match);
                ToastBar.showMessage("Removed successfully !", FontImage.MATERIAL_DONE);
                FavoriteForm favoriteForm = new FavoriteForm();
                favoriteForm.getForm().show();

            }


        });
        stadiumContainer.add(BorderLayout.EAST, lblClose);
        Container levelContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        levelContainer.add(BorderLayout.CENTER, new Label(match.getLevel().toUpperCase()));

        upperHolder.add(stadiumContainer);
        upperHolder.add(levelContainer);

        Container lowerHolder = new Container(new BorderLayout());
        ((BorderLayout) lowerHolder.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);

        Container firstTeamContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        firstTeamContainer.getStyle().setMargin(30, 30, 50, 30);
        firstTeamContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        firstTeamContainer.getStyle().setMargin(30, 30, 30, 50);
        firstTeamContainer.getStyle().setPadding(30, 30, 30, 30);


        EncodedImage imgHolder = EncodedImage.create("/giphy.gif");
        URLImage image =  URLImage.createToStorage(imgHolder,match.getFirstTeam().getTeamLogo(),
                Constant.PATH+Constant.TEAM_DIR+match.getFirstTeam().getTeamLogo());

        firstTeamContainer.add(BorderLayout.CENTER,image);
        Container shortCutContainer1 = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        shortCutContainer1.add(BorderLayout.CENTER,new Label(match.getFirstTeam().getTeamShortcut().toUpperCase()));
        firstTeamContainer.add(BorderLayout.SOUTH,shortCutContainer1);

        Container centerContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label date = new Label(new SimpleDateFormat("E d, MMM").format(match.getDates()).toUpperCase());
        date.getAllStyles().setFgColor(0xff0000);
        centerContainer.add(date);

        Button btnBuyTicket = new Button("Buy Ticket");
        btnBuyTicket.getAllStyles().setFgColor(0xff0000);
        btnBuyTicket.getAllStyles().setBorder(Border.createLineBorder(1, rgb(220,220,220)));
        centerContainer.add(btnBuyTicket);
        btnBuyTicket.addActionListener(e->{
            PaymentForm paymentForm = new PaymentForm(match,form);
            paymentForm.getForm().show();
        });

        Container btnHolder = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        Button shareButton = new Button();
        btnHolder.add(BorderLayout.CENTER,shareButton);
        centerContainer.add(btnHolder);


        Container secondTeamContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        secondTeamContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        secondTeamContainer.getStyle().setMargin(30, 30, 30, 50);
        secondTeamContainer.getStyle().setPadding(30, 30, 30, 30);

        URLImage image1 =  URLImage.createToStorage(imgHolder, match.getSecondTeam().getTeamLogo(),
                Constant.PATH+Constant.TEAM_DIR+match.getSecondTeam().getTeamLogo());
        secondTeamContainer.add(BorderLayout.CENTER,image1);
        Container shortCutContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        shortCutContainer.add(BorderLayout.CENTER,new Label(match.getSecondTeam().getTeamShortcut().toUpperCase()));
        secondTeamContainer.add(BorderLayout.SOUTH,shortCutContainer);



        lowerHolder.add(BorderLayout.WEST, firstTeamContainer);
        lowerHolder.add(BorderLayout.CENTER, centerContainer);
        lowerHolder.add(BorderLayout.EAST, secondTeamContainer);

        holder.add(upperHolder);
        holder.add(lowerHolder);

        return holder;
    }

}
