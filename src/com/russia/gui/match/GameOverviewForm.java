package com.russia.gui.match;


import com.codename1.components.Accordion;
import com.codename1.components.SpanLabel;
import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.match.Event;
import com.russia.entities.match.Score;
import com.russia.services.match.EventService;
import com.russia.utils.Constant;
import com.russia.utils.Statistics;

import java.io.IOException;
import java.util.List;

import static com.codename1.charts.util.ColorUtil.rgb;
import static java.util.Arrays.asList;


public class GameOverviewForm {
    Form form;
    Resources theme;
    static Statistics firstTeamStat;
    static Statistics secondTeamStat ;



    public GameOverviewForm(String string, Score score) {
        theme = UIManager.initFirstTheme("/theme");
        form = new Form(string, new BoxLayout(BoxLayout.Y_AXIS));
        form.getStyle().setBgColor(rgb(245, 245, 245));
        try {
            form.add(addScoreContainer(score));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Container container = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        container.add(BorderLayout.CENTER, new Label("Game Statistics".toUpperCase()));
        container.getStyle().setMargin(20, 20, 20, 20);
        form.add(container);
        form.add(addStatisticsContainer(score));
        form.add(addEventContainer(score));
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    private Container addScoreContainer(Score score) throws IOException {
        Container holder = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        holder.getStyle().setBgColor(rgb(255, 255, 255));
        holder.getStyle().setBgTransparency(255);
        holder.getStyle().setMargin(20, 10, 20, 20);


        Container upperHolder = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Container stadiumContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        Label stadium = new Label(score.getMatch().getStadium().toUpperCase());
        stadium.getAllStyles().setFgColor(rgb(169, 169, 169));
        stadiumContainer.add(BorderLayout.CENTER, stadium);

        Container levelContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        levelContainer.add(BorderLayout.CENTER, new Label(score.getMatch().getLevel().toUpperCase()));

        upperHolder.add(stadiumContainer);
        upperHolder.add(levelContainer);

        Container lowerHolder = new Container(new BorderLayout());
        ((BorderLayout) lowerHolder.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);

        Container firstTeamContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        firstTeamContainer.getStyle().setMargin(30, 30, 50, 30);
        firstTeamContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        firstTeamContainer.getStyle().setMargin(30, 30, 30, 50);
        firstTeamContainer.getStyle().setPadding(30, 30, 30, 30);


        EncodedImage imgHolder = EncodedImage.create("/giphy.gif");
        URLImage image =  URLImage.createToStorage(imgHolder, score.getMatch().getFirstTeam().getTeamLogo(),
                Constant.PATH+ Constant.TEAM_DIR+score.getMatch().getFirstTeam().getTeamLogo());

        firstTeamContainer.add(BorderLayout.CENTER,image);
        Container shortCutContainer1 = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        shortCutContainer1.add(BorderLayout.CENTER,new Label(score.getMatch().getFirstTeam().getTeamShortcut().toUpperCase()));
        firstTeamContainer.add(BorderLayout.SOUTH,shortCutContainer1);
        Container centerContainer = new Container(new BorderLayout());
        ((BorderLayout) centerContainer.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);

        Label scoreT1 = new Label(String.valueOf(score.getScoreTeam1()));
        Label scoreT2 = new Label(String.valueOf(score.getScoreTeam2()));
        if (score.getScoreTeam1() > score.getScoreTeam2()) {
            scoreT1.getAllStyles().setFgColor(0xff0000);
        } else if (score.getScoreTeam1() < score.getScoreTeam2()) {
            scoreT2.getAllStyles().setFgColor(0xff0000);
        }
        centerContainer.add(BorderLayout.WEST, scoreT1);
        centerContainer.add(BorderLayout.CENTER, new Label(" -- "));
        centerContainer.add(BorderLayout.EAST, scoreT2);


        Container secondTeamContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        secondTeamContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        secondTeamContainer.getStyle().setMargin(30, 30, 30, 50);
        secondTeamContainer.getStyle().setPadding(30, 30, 30, 30);

        URLImage image1 =  URLImage.createToStorage(imgHolder, score.getMatch().getSecondTeam().getTeamLogo(),
                Constant.PATH+ Constant.TEAM_DIR+score.getMatch().getSecondTeam().getTeamLogo());
        secondTeamContainer.add(BorderLayout.CENTER,image1);
        Container shortCutContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        shortCutContainer.add(BorderLayout.CENTER,new Label(score.getMatch().getSecondTeam().getTeamShortcut().toUpperCase()));
        secondTeamContainer.add(BorderLayout.SOUTH,shortCutContainer);


        lowerHolder.add(BorderLayout.WEST, firstTeamContainer);
        lowerHolder.add(BorderLayout.CENTER, centerContainer);
        lowerHolder.add(BorderLayout.EAST, secondTeamContainer);

        holder.add(upperHolder);
        holder.add(lowerHolder);

        return holder;

    }

    private Container addStatisticsContainer(Score score) {
        EventService eventService = new EventService();
        List<Event> events = eventService.getListEventByMatch(score);
        firstTeamStat = new Statistics();
        secondTeamStat = new Statistics();
        for (Event event : events) {
            if (event.getIdTeam() == score.getMatch().getFirstTeam().getId()) {
                firstTeamStat.dataFormat(event);
            } else {
                secondTeamStat.dataFormat(event);
            }
        }
        return createHolder(firstTeamStat,secondTeamStat);
    }

    public Container createHolder(Statistics firstTeamStat, Statistics secondTeamStat){
        List<String> stats = asList("SHOTS(ON TARGET)", "CORNER KICKS", "FOULS", "YELLOW CARDS", "RED CARDS");
        Container holder = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        holder.getStyle().setMargin(20, 20, 20, 20);
        holder.getStyle().setBgColor(rgb(255, 255, 255));
        holder.getStyle().setBgTransparency(255);
        holder.getStyle().setPadding(50, 50, 50, 50);

        for (String s : stats) {
            Label firstTeam = new Label("0");
            firstTeam.getAllStyles().setFgColor(0xffffff);
            Label secondTeam = new Label();
            secondTeam.getAllStyles().setFgColor(0xffffff);
            if (s.equals("SHOTS(ON TARGET)")) {
                firstTeam.setText(String.valueOf(firstTeamStat.getShot()));
                secondTeam.setText(String.valueOf(secondTeamStat.getShot()));
            } else if (s.equals("CORNER KICKS")) {
                firstTeam.setText(String.valueOf(firstTeamStat.getCornerKick()));
                secondTeam.setText(String.valueOf(secondTeamStat.getCornerKick()));
            } else if (s.equals("FOULS")) {
                firstTeam.setText(String.valueOf(firstTeamStat.getFoul()));
                secondTeam.setText(String.valueOf(secondTeamStat.getFoul()));
            } else if (s.equals("YELLOW CARDS")) {
                firstTeam.setText(String.valueOf(firstTeamStat.getYellowCard()));
                secondTeam.setText(String.valueOf(secondTeamStat.getRedCard()));
            } else if (s.equals("RED CARDS")) {
                firstTeam.setText(String.valueOf(firstTeamStat.getRedCard()));
                secondTeam.setText(String.valueOf(secondTeamStat.getRedCard()));
            }

            Container container = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
            container.getStyle().setMargin(0, 30, 0, 0);

            Container left = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
            left.getStyle().setBgColor(0x2c2d31);
            left.getStyle().setBgTransparency(255);
            left.getStyle().setPadding(20, 20, 60, 60);
            left.add(BorderLayout.CENTER, firstTeam);

            GridLayout gridLayout = new GridLayout(1, 1);
            gridLayout.setAutoFit(true);
            Container center = new Container(gridLayout);
            center.getStyle().setPadding(20, 20, 150, 0);
            center.getStyle().setBgColor(rgb(245, 245, 245));
            center.getStyle().setBgTransparency(255);
            center.add(new Label(s));

            Container right = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
            right.getStyle().setBgColor(0x2c2d31);
            right.getStyle().setBgTransparency(255);
            right.getStyle().setPadding(20, 20, 60, 60);
            right.add(BorderLayout.CENTER, secondTeam);

            container.add(BorderLayout.WEST, left);
            container.add(BorderLayout.CENTER, center);
            container.add(BorderLayout.EAST, right);

            holder.add(container);
        }
        return holder;
    }

    public Container addEventContainer(Score score){
        EventService eventService = new EventService();
        List<Event> listEvent = eventService.getListEventByMatch(score);
        Container holder = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        holder.getStyle().setMargin(20, 20, 20, 20);
        holder.getStyle().setBgColor(rgb(255, 255, 255));
        holder.getStyle().setBgTransparency(255);
        holder.getStyle().setPadding(50, 50, 50, 50);
        for (Event event : listEvent){
            holder.add(new SpanLabel(event.getMinutes()+"’  "+event.getTypeEvent().toUpperCase()+"\n     "+
                    event.getDescription()));
        }
        return holder;

    }
}
