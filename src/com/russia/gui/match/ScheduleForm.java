package com.russia.gui.match;



import com.codename1.components.ShareButton;
import com.codename1.components.ToastBar;
import com.codename1.messaging.Message;
import com.codename1.ui.*;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.dao.MatchDAO;
import com.russia.entities.match.Match;
import com.russia.gui.HomeForm;
import com.russia.gui.payment.PaymentForm;
import com.russia.services.match.MatchService;
import com.russia.utils.Constant;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import static com.codename1.charts.util.ColorUtil.rgb;

public class ScheduleForm {
    Form form;
    private Resources theme;
    static Label lblGrey, lblRed;
    static MatchDAO matchDAO;
    static ArrayList<Match> listOfFavoriteMatch;



    public ScheduleForm() {

        theme = UIManager.initFirstTheme("/theme");
        form = new Form("Schedule", new BoxLayout(BoxLayout.Y_AXIS));
        form.getStyle().setBgColor(rgb(245, 245, 245));
     /*   form.getToolbar().addMaterialCommandToLeftBar(null, FontImage.MATERIAL_ARROW_BACK, evt ->{
                HomeForm homeForm = new HomeForm();
                homeForm.getForm().show();
        });
*/
        form.getToolbar().addMaterialCommandToOverflowMenu("My Games",FontImage.MATERIAL_FAVORITE,evt -> {

            FavoriteForm favoriteForm = new FavoriteForm();
            favoriteForm.getForm().show();

        });
        MatchService matchService = new MatchService();
        matchDAO = new MatchDAO();
        listOfFavoriteMatch = matchDAO.getFavoriteMatch();
        for (Match match : matchService.getListMatch()) {
            try {
                form.add(addItem(match));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }


    private Container addItem(Match match) throws IOException {

        Container holder = new Container(BoxLayout.y());
        holder.getStyle().setBgColor(rgb(255, 255, 255));
        holder.getStyle().setBgTransparency(255);
        holder.getStyle().setMargin(20, 10, 20, 20);

        Container upperHolder = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Container stadiumContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        Label stadium = new Label(match.getStadium().toUpperCase());
        stadium.getAllStyles().setFgColor(rgb(169, 169, 169));
        stadiumContainer.add(BorderLayout.CENTER, stadium);

        Image redHeart = theme.getImage("favorite_red.png");
        Image greyHeart = theme.getImage("favorite_grey.png");
        if(exist(listOfFavoriteMatch,match)){
            lblRed = new Label(redHeart);
            stadiumContainer.add(BorderLayout.EAST, lblRed);
            lblRed.addPointerPressedListener(evt -> {
                if (
                        Dialog.show(match.getFirstTeam().getTeamShortcut().toUpperCase() + " vs " + match.getSecondTeam().getTeamShortcut().toUpperCase(),
                                "Do you really want to remove this game from your favorite ?", "Yes", "Cancel")) {
                    MatchDAO matchDAO = new MatchDAO();
                    matchDAO.deleteMatch(match);
                    ToastBar.showMessage("Removed successfully !", FontImage.MATERIAL_DONE);
                    ScheduleForm scheduleForm = new ScheduleForm();
                    scheduleForm.getForm().show();

                }
            });
        }else{
            lblGrey = new Label(greyHeart);
            stadiumContainer.add(BorderLayout.EAST, lblGrey);
            lblGrey.addPointerPressedListener(e -> {
                if (
                        Dialog.show(match.getFirstTeam().getTeamName().toUpperCase() + " vs " + match.getSecondTeam().getTeamName().toUpperCase(),
                                "Do you want to add this game to your favorite ?", "Yes", "Cancel")) {
                    matchDAO.addMatchToFavorite(match);
                    ToastBar.showMessage("Added successfully !", FontImage.MATERIAL_DONE);
                    HomeForm homeForm = new HomeForm();
                    homeForm.getForm().show();
                }
            });
        }
        Container levelContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        levelContainer.add(BorderLayout.CENTER, new Label(match.getLevel().toUpperCase()));

        upperHolder.add(stadiumContainer);
        upperHolder.add(levelContainer);

        Container lowerHolder = new Container(new BorderLayout());
        ((BorderLayout) lowerHolder.getLayout()).setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);

        Container firstTeamContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        firstTeamContainer.getStyle().setMargin(30, 30, 50, 30);
        firstTeamContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        firstTeamContainer.getStyle().setMargin(30, 30, 30, 50);
        firstTeamContainer.getStyle().setPadding(30, 30, 30, 30);


        EncodedImage imgHolder = EncodedImage.create("/giphy.gif");
        URLImage image =  URLImage.createToStorage(imgHolder, match.getFirstTeam().getTeamLogo(),
                Constant.PATH+Constant.TEAM_DIR+match.getFirstTeam().getTeamLogo());

        firstTeamContainer.add(BorderLayout.CENTER,image);
        Container shortCutContainer1 = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        shortCutContainer1.add(BorderLayout.CENTER,new Label(match.getFirstTeam().getTeamShortcut().toUpperCase()));
        firstTeamContainer.add(BorderLayout.SOUTH,shortCutContainer1);

        Container centerContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label date = new Label(new SimpleDateFormat("E d, MMM").format(match.getDates()).toUpperCase());
        date.getAllStyles().setFgColor(0xff0000);
        centerContainer.add(date);
        Button btnBuyTicket = new Button("Buy Ticket");
        btnBuyTicket.getAllStyles().setFgColor(0xff0000);
        btnBuyTicket.getAllStyles().setBorder(Border.createLineBorder(1, rgb(220,220,220)));
        centerContainer.add(btnBuyTicket);
        // Buy ticket implementation
        btnBuyTicket.addActionListener(event -> {
            PaymentForm paymentForm = new PaymentForm(match, form);
            paymentForm.getForm().show();
        });

        Container btnHolder = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        Image facebookIcon = theme.getImage("facebook-box.png");
        Label lblFacebookIcon = new Label(facebookIcon);
        lblFacebookIcon.getAllStyles().setFgColor(0xff0000);
        btnHolder.add(BorderLayout.CENTER,lblFacebookIcon);
        lblFacebookIcon.addPointerPressedListener(e->{
           /* Message m = new Message("Ne rater pas le match : " +match.getFirstTeam().getTeamName()+ " VS " + match.getSecondTeam().getTeamName() + " le "
                    +new SimpleDateFormat("d-MM-yyyy").format(match.getDates()) + " à " +match.getTime());
            m.getAttachments().put("", "text/plain");
            m.getAttachments().put("", "image/png");
            Display.getInstance().sendMessage(new String[]{"ahlembenguiza@gmail.com"}, "Alert",,m);
       /*     FacebookClient facebookClient = new DefaultFacebookClient(Constant.SECRET_TOKEN);
            FacebookType facebookType= facebookClient.publish("me/feed", FacebookType.class, Parameter.with("message","bbb"));
            System.out.println("fb.com" + facebookType.getId());*/
        });
        centerContainer.add(btnHolder);

        Container secondTeamContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        secondTeamContainer.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
        secondTeamContainer.getStyle().setMargin(30, 30, 30, 50);
        secondTeamContainer.getStyle().setPadding(30, 30, 30, 30);

        URLImage image1 =  URLImage.createToStorage(imgHolder, match.getSecondTeam().getTeamLogo(),
                Constant.PATH+Constant.TEAM_DIR+match.getSecondTeam().getTeamLogo());
        secondTeamContainer.add(BorderLayout.CENTER,image1);
        Container shortCutContainer = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
        shortCutContainer.add(BorderLayout.CENTER,new Label(match.getSecondTeam().getTeamShortcut().toUpperCase()));
        secondTeamContainer.add(BorderLayout.SOUTH,shortCutContainer);


        lowerHolder.add(BorderLayout.WEST, firstTeamContainer);
        lowerHolder.add(BorderLayout.CENTER, centerContainer);
        lowerHolder.add(BorderLayout.EAST, secondTeamContainer);

        holder.add(upperHolder);
        holder.add(lowerHolder);

        return holder;
    }

    private Boolean exist (ArrayList<Match> list, Match match){
       Boolean bool = false;
        Iterator iterator = list.iterator();
        while(iterator.hasNext() && !bool ){
            Match m = (Match) iterator.next();
            if(match.getId() == m.getId()){
                bool = true;
            }
        }
        return bool;
    }
}
