package com.russia.gui.Group;

import com.codename1.components.ImageViewer;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.*;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.russia.entities.Group.Groupe;
import com.russia.services.GroupService;

import java.io.IOException;
import java.util.ArrayList;

public class GroupListForm2 {


    Form f2 = new Form("Group B", new BoxLayout(BoxLayout.Y_AXIS));

    Label nomTeam1 = new Label();
    Label nomTeam2 = new Label();
    Label nomTeam3 = new Label();
    Label nomTeam4 = new Label();
    private EncodedImage image;
    private Image img1;
    private Image img2;
    private Image img3;
    private Image img4;

    public GroupListForm2() {
        System.out.println("form 2");
        ArrayList<Groupe> list = new ArrayList<Groupe>();
        ArrayList<Groupe> listfinale = new ArrayList<Groupe>();
        GroupService serviceGroup = new GroupService();
        list = serviceGroup.getList2();
        for (Groupe group : list) {
            if (group.getNameGroup().equals("B")) {
                listfinale.add(group);
            }
        }
        for (Groupe group : listfinale) {
            try {
                //System.out.println(theme.toString());
                Container contImage = new Container(new BoxLayout(BoxLayout.X_AXIS_NO_GROW));
                Container contenu = new Container(new FlowLayout(Component.CENTER, Component.TOP));
                Container Big = new Container(new FlowLayout(Component.CENTER));

                image = EncodedImage.create("/giphy.gif");
                img1 = URLImage.createToStorage(image, group.getId_team1().getTeamLogo(), "http://localhost//RUSSIA//web//assets//images//teamUploads//" + group.getId_team1().getTeamLogo());
                //img1 = img1.scaled(300, 300);
                Big.add(img1);

                img2 = URLImage.createToStorage(image, group.getId_team2().getTeamLogo(), "http://localhost//RUSSIA//web//assets//images//teamUploads//" + group.getId_team2().getTeamLogo());
                //img2 = img2.scaled(50, 50);
                Big.add(img2);
                img3 = URLImage.createToStorage(image, group.getId_team3().getTeamLogo(), "http://localhost//RUSSIA//web//assets//images//teamUploads//" + group.getId_team3().getTeamLogo());
                //img3 = img3.scaled(50, 50);
                Big.add(img3);
                img4 = URLImage.createToStorage(image, group.getId_team4().getTeamLogo(), "http://localhost//RUSSIA//web//assets//images//teamUploads//" + group.getId_team4().getTeamLogo());
                //img4 = img4.scaled(50, 50);
                Big.add(img4);

                ImageViewer imgv1 = new ImageViewer(img1);
                // Image img = theme.getImaage(img1).ScaledWidth(Math.round(Display.getInstance().getDisplayWidth()/10));
                ImageViewer imgv2 = new ImageViewer(img2);
                ImageViewer imgv3 = new ImageViewer(img3);
                ImageViewer imgv4 = new ImageViewer(img4);

                nomTeam1.setText(group.getId_team1().getTeamName());

                nomTeam2.setText(group.getId_team2().getTeamName());
                nomTeam3.setText(group.getId_team3().getTeamName());
                nomTeam4.setText(group.getId_team4().getTeamName());
                System.out.println(nomTeam1.getText());
                System.out.println(nomTeam2.getText());
                System.out.println(nomTeam3.getText());
                System.out.println(nomTeam4.getText());
                f2.add(nomTeam1);
                f2.add(imgv1);
                f2.add(nomTeam2);
                f2.add(imgv2);
                f2.add(nomTeam3);
                f2.add(imgv3);
                f2.add(nomTeam4);
                f2.add(imgv4);
                Big.add(contenu);
                // f2.add(FlowLayout.encloseCenter(createStarRankSlider()));



            } catch (IOException ex) {
                ex.getMessage();
            }

        }
        Slider starRank = new Slider();
        starRank.setEditable(true);
        starRank.setMinValue(0);
        starRank.setMaxValue(5);
        // starRank.getProgress();
        Font fnt = Font.createTrueTypeFont("native:MainLight", "native:MainLight").
                derive(Display.getInstance().convertToPixels(5, true), Font.STYLE_PLAIN);
        Style s = new Style(0xffff33, 0, fnt, (byte) 0);
        Image fullStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
        s.setOpacity(100);
        s.setFgColor(0);
        Image emptyStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
        initStarRankStyle(starRank.getSliderEmptySelectedStyle(), emptyStar);
        initStarRankStyle(starRank.getSliderEmptyUnselectedStyle(), emptyStar);
        initStarRankStyle(starRank.getSliderFullSelectedStyle(), fullStar);
        initStarRankStyle(starRank.getSliderFullUnselectedStyle(), fullStar);
        starRank.setPreferredSize(new Dimension(fullStar.getWidth() * 5, fullStar.getHeight()));
        starRank.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                int ratingVal = starRank.getProgress();
                System.out.println("rating value:" + ratingVal);
                int id = 0;
                for (Groupe group : listfinale) {
                    id = group.getId();
                }
                System.out.println("id groupe:" + id);
                ConnectionRequest con = new ConnectionRequest();
                con.setUrl("http://localhost/RUSSIA/web/app_dev.php/group/groupe/new/" + id + "/" + ratingVal + "/2");
                con.addResponseListener(networkEvent -> {


                });
                con.setFailSilently(true);
                NetworkManager.getInstance().addToQueueAndWait(con);
            }
        });

        f2.add(FlowLayout.encloseCenter(starRank));

    }

    private void initStarRankStyle(Style s, Image star) {
        s.setBackgroundType(Style.BACKGROUND_IMAGE_TILE_BOTH);
        s.setBorder(Border.createEmpty());
        s.setBgImage(star);
        s.setBgTransparency(0);
    }

    private Slider createStarRankSlider() {
        Slider starRank = new Slider();
        starRank.setEditable(true);
        starRank.setMinValue(0);
        starRank.setMaxValue(10);
        Font fnt = Font.createTrueTypeFont("native:MainLight", "native:MainLight").
                derive(Display.getInstance().convertToPixels(5, true), Font.STYLE_PLAIN);
        Style s = new Style(0xffff33, 0, fnt, (byte) 0);
        Image fullStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
        s.setOpacity(100);
        s.setFgColor(0);
        Image emptyStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
        initStarRankStyle(starRank.getSliderEmptySelectedStyle(), emptyStar);
        initStarRankStyle(starRank.getSliderEmptyUnselectedStyle(), emptyStar);
        initStarRankStyle(starRank.getSliderFullSelectedStyle(), fullStar);
        initStarRankStyle(starRank.getSliderFullUnselectedStyle(), fullStar);
        starRank.setPreferredSize(new Dimension(fullStar.getWidth() * 5, fullStar.getHeight()));
        return starRank;
    }


    public Form getF2() {
        return f2;
    }

    public void setF2(Form f2) {
        this.f2 = f2;
    }
}
