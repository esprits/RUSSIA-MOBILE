package com.russia.gui.Group;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.*;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.russia.entities.Group.Groupe;
import com.russia.entities.team.Team;
import com.russia.services.GroupService;
import com.russia.services.TeamService;
import com.codename1.ui.Slider;

import java.io.IOException;
import java.util.ArrayList;

import static com.codename1.charts.util.ColorUtil.rgb;

public class GroupListForm1 {

    Form form;
    private EncodedImage image;
    private Image img1;
    private Image img2;
    private Image img3;
    private Image img4;
    private Resources theme;

    public GroupListForm1() {

        theme = UIManager.initFirstTheme("/theme");
        this.form = new Form("Groups", BoxLayout.y());
        form.getStyle().setBgColor(rgb(245, 245, 245));
        GroupService groupService = new GroupService();
        for (Groupe groupe : groupService.getList2()) {
            form.add(addItem(groupe));
        }
    }


    private Container addItem(Groupe groupe) {

        Container components = new Container(BoxLayout.y());
        Label label = new Label(groupe.getNameGroup().toUpperCase());
        components.add(label);
        try {

            Container teamContainer1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
            teamContainer1.getStyle().setMargin(30, 30, 50, 30);
            teamContainer1.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
            teamContainer1.getStyle().setMargin(30, 30, 30, 50);
            teamContainer1.getStyle().setPadding(30, 30, 30, 30);
            image = EncodedImage.create("/giphy.gif");
            img1 = URLImage.createToStorage(image, groupe.getId_team1().getTeamLogo(), "http://localhost//RUSSIA//web//assets//images//teamUploads//" + groupe.getId_team1().getTeamLogo());
            img1 = img1.scaled(200, 200);
            teamContainer1.add(img1);
            Label label2 = new Label(groupe.getId_team1().getTeamName().toUpperCase());
            teamContainer1.add(label2);

            Container teamContainer2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
            teamContainer2.getStyle().setMargin(30, 30, 50, 30);
            teamContainer2.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
            teamContainer2.getStyle().setMargin(30, 30, 30, 50);
            teamContainer2.getStyle().setPadding(30, 30, 30, 30);
            img2 = URLImage.createToStorage(image, groupe.getId_team2().getTeamLogo(), "http://localhost//RUSSIA//web//assets//images//teamUploads//" + groupe.getId_team2().getTeamLogo());
            img2 = img2.scaled(200, 200);
            Label label3 = new Label(groupe.getId_team2().getTeamName().toUpperCase());
            teamContainer2.add(img2);
            teamContainer2.add(label3);

            Container teamContainer3 = new Container(new BoxLayout(BoxLayout.X_AXIS));
            teamContainer3.getStyle().setMargin(30, 30, 50, 30);
            teamContainer3.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
            teamContainer3.getStyle().setMargin(30, 30, 30, 50);
            teamContainer3.getStyle().setPadding(30, 30, 30, 30);
            img3 = URLImage.createToStorage(image, groupe.getId_team3().getTeamLogo(), "http://localhost//RUSSIA//web//assets//images//teamUploads//" + groupe.getId_team3().getTeamLogo());
            img3 = img3.scaled(200, 200);
            Label label4 = new Label(groupe.getId_team3().getTeamName().toUpperCase());
            teamContainer3.add(img3);
            teamContainer3.add(label4);

            Container teamContainer4 = new Container(new BoxLayout(BoxLayout.X_AXIS));
            teamContainer4.getStyle().setMargin(30, 30, 50, 30);
            teamContainer4.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
            teamContainer4.getStyle().setMargin(30, 30, 30, 50);
            teamContainer4.getStyle().setPadding(30, 30, 30, 30);
            img4 = URLImage.createToStorage(image, groupe.getId_team4().getTeamLogo(), "http://localhost//RUSSIA//web//assets//images//teamUploads//" + groupe.getId_team4().getTeamLogo());
            img4 = img4.scaled(200, 200);
            Label label5 = new Label(groupe.getId_team4().getTeamName().toUpperCase());
            teamContainer4.add(img4);
            teamContainer4.add(label5);


            components.getStyle().setBgColor(rgb(255, 255, 255));
            components.getStyle().setBgTransparency(255);
            components.getStyle().setBorder(Border.createLineBorder(0, rgb(220, 220, 220)));
            components.getStyle().setMargin(10, 10, 20, 20);

            components.add(teamContainer1);
            components.add(teamContainer2);
            components.add(teamContainer3);
            components.add(teamContainer4);
            components.setLeadComponent(label);


        } catch (IOException e) {

        }
        return components;


    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }
}
