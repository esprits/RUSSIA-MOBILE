 package com.russia.payment;


import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class StripeClient {

    public static final String stripeKey = "sk_test_cmHlewUEjcBi8FuyxxRLFEQN";

    private String token = "tok_mastercard";
    public StripeClient() {

    }

    public void charge(double amount, String description) throws IOException {
        ConnectionRequest connectionRequest = new ConnectionRequest();
        connectionRequest.setPost(true);
        connectionRequest.addRequestHeader("Authorization", String.format("Bearer %s", stripeKey));

        connectionRequest.addArgument("amount", "10000");
        connectionRequest.addArgument("currency", "usd");
        connectionRequest.addArgument("source", "tok_mastercard");
        connectionRequest.addArgument("description", description);

        connectionRequest.setUrl("https://api.stripe.com/v1/charges");
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        byte[] responseData = connectionRequest.getResponseData();

        if (responseData == null) {
            throw new IOException("Network Err");
        }

    }

    public String createToken(CreditCard card) throws IOException {
        ConnectionRequest connectionRequest = new ConnectionRequest();
        connectionRequest.setPost(true);
        connectionRequest.addRequestHeader("Authorization", String.format("Bearer %s", stripeKey));

        connectionRequest.addArgument("card[number]", card.getCardNumber());
        connectionRequest.addArgument("card[exp_month]", String.valueOf(card.getExpMonth()));
        connectionRequest.addArgument("card[exp_year]", String.valueOf(card.getExpYear()));
        connectionRequest.addArgument("card[cvc]", card.getCvc());

        connectionRequest.setUrl("https://api.stripe.com/v1/tokens");
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        byte[] responseData = connectionRequest.getResponseData();

        if (responseData == null) {
            throw new IOException("Network Err");
        }

        Map<String, Object> response = getStringObjectMap(responseData);



        this.token = (String) response.get("id");

        return token;
    }

    protected Map<String, Object> getStringObjectMap(byte[] data) throws IOException {
        JSONParser parser = new JSONParser();
        return parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
    }


}
