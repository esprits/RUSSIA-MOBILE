package com.russia.payment;


public class CreditCard {
    String cardNumber;
    int expMonth;
    int expYear;
    String cvc;


    public CreditCard() {

    }

    public CreditCard(String cardNumber, int expMonth, int expYear, String cvc) {
        this.cardNumber = cardNumber;
        this.expMonth = expMonth;
        this.expYear = expYear;
        this.cvc = cvc;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public CreditCard setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public CreditCard setExpMonth(int expMonth) {
        this.expMonth = expMonth;
        return this;
    }

    public int getExpYear() {
        return expYear;
    }

    public CreditCard setExpYear(int expYear) {
        this.expYear = expYear;
        return this;
    }


    public String getCvc() {
        return cvc;
    }

    public CreditCard setCvc(String cvc) {
        this.cvc = cvc;
        return this;
    }
}
