package com.russia.utils;

import com.codename1.db.Database;
import com.codename1.ui.Display;

import java.io.IOException;

public class DataBaseManager {
    static Database database = null;

    public static Database getDatabase() {
        return database;
    }

    public static void setDatabase(Database database) {
        DataBaseManager.database = database;
    }

    public static Database getConnexion(){
        boolean created = false;
        created = Display.getInstance().exists("matchs");
        try{
            database = Display.getInstance().openOrCreate("matchs");
            if (!created){
                database.execute("create Table Team (id INTEGER, shortcut TEXT, logo TEXT)");
                database.execute("create table Match (id INTEGER, " +
                        "firstTeam INTEGER, secondTeam INTEGER," +
                        "stadium TEXT, level TEXT, date TEXT , time TEXT,"+
                        " FOREIGN KEY(firstTeam) REFERENCES Team(id)," +
                        " FOREIGN KEY(secondTeam) REFERENCES Team(id))" );
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return database;
    }
}
