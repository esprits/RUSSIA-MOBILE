package com.russia.utils;


import java.text.DecimalFormat;
import java.text.ParseException;

public class LocationFormatter {


    private static DecimalFormat formatter = new DecimalFormat("##.00000000");

    public static String format(Double number) {
        return formatter.format(number);
    }

    public static Double parse(String text) {
        try {
            return (Double) formatter.parse(text);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0.0;
    }

//    public static LatLong format(String longitude, String latitude) {
//        return new LatLong(parse(longitude), parse(latitude));
//    }
//
//    public static LatLong format(GeoCode geoCode) {
//        return new LatLong(parse(geoCode.getLongitude()), parse(geoCode.getLatitude()));
//    }
}

