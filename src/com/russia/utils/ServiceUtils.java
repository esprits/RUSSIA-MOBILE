package com.russia.utils;

import com.russia.entities.guide.Place;
import com.russia.entities.guide.Region;
import com.russia.entities.ticket.Ticket;
import com.russia.services.PlaceService;
import com.russia.services.RegionService;
import com.russia.services.TicketService;

import java.io.IOException;
import java.util.List;

public class ServiceUtils {
    public static List<Region> getRegions() {
        RegionService regionService = new RegionService();
        try {
            return regionService.getAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Place getPlace(int id) {
        PlaceService placeService = new PlaceService();
        try {
            return placeService.get(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Ticket getTicket(int id) {
        TicketService ticketService = new TicketService();
        try {
            return ticketService.get(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
