package com.russia.utils;

import com.russia.entities.match.Event;

public class Statistics {
    private int shot = 0;
    private int cornerKick = 0;
    private int foul = 0;
    private int yellowCard = 0;
    private int redCard = 0;

    public int getShot() {
        return shot;
    }

    public void setShot(int shot) {
        this.shot = shot;
    }

    public int getCornerKick() {
        return cornerKick;
    }

    public void setCornerKick(int cornerKick) {
        this.cornerKick = cornerKick;
    }

    public int getFoul() {
        return foul;
    }

    public void setFoul(int foul) {
        this.foul = foul;
    }

    public int getYellowCard() {
        return yellowCard;
    }

    public void setYellowCard(int yellowCard) {
        this.yellowCard = yellowCard;
    }


    public int getRedCard() {
        return redCard;
    }

    public void setRedCard(int redCard) {
        this.redCard = redCard;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "shot=" + shot +
                ", cornerKick=" + cornerKick +
                ", foul=" + foul +
                ", yellowCard=" + yellowCard +
                ", redCard=" + redCard +
                '}';
    }


    public void dataFormat(Event event) {
        if (event.getTypeEvent().equals("Shot(On Target)")) {
            this.shot += 1;
        } else if (event.getTypeEvent().equals("Corner Kick")) {
            this.cornerKick += 1;
        } else if (event.getTypeEvent().equals("Foul")) {
            this.foul += 1;
        } else if (event.getTypeEvent().equals("Yellow Card")) {
            this.yellowCard += 1;
        } else if (event.getTypeEvent().equals("Red Card")) {
            this.redCard += 1;
        }else if(event.getTypeEvent().equals("Goal")){
            this.shot += 1;
        }
    }
}

