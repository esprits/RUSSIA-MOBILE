package com.russia.utils;


import java.text.DecimalFormat;
import java.text.ParseException;

public class IdentityFormatter {


    private static DecimalFormat formatter = new DecimalFormat("#######.#");

    public static String format(Double number) {
        return formatter.format(number);
    }

    public static Integer parse(String text) {
        try {
            return Integer.parseInt(String.valueOf(formatter.parse(text)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

//    public static LatLong format(String longitude, String latitude) {
//        return new LatLong(parse(longitude), parse(latitude));
//    }
//
//    public static LatLong format(GeoCode geoCode) {
//        return new LatLong(parse(geoCode.getLongitude()), parse(geoCode.getLatitude()));
//    }
}

