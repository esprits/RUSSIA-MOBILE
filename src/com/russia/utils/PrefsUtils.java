package com.russia.utils;

import com.codename1.io.Preferences;
import com.codename1.io.Storage;

import java.util.Hashtable;

public class PrefsUtils {


    private static String preferencesLocation = Preferences.getPreferencesLocation();
    private static Hashtable<String, Object> p;

    private PrefsUtils() {
    }


    public static String getValue(String name, String value) {
        return Preferences.get(name, (value == null || value.isEmpty()) ? "" : value);
    }


    public static void setValue(String name, String value) {
        Preferences.set(name, (value == null || value.isEmpty()) ? "" : value);
    }

    public static void removeValue(String value){
        Preferences.delete(value);
    }


    public static int getValue(String name, int value) {
        return Preferences.get(name, value);
    }

    public static void setValue(String name, int value) {
        Preferences.set(name, value);
    }


    public static Hashtable<String, Object> getAll() {
        if (p == null) {
            if (Storage.getInstance().exists(preferencesLocation)) {
                p = (Hashtable<String, Object>) Storage.getInstance().readObject(preferencesLocation);
            }
            if (p == null) {
                p = new Hashtable<String, Object>();
            }
        }
        return p;
    }
}
