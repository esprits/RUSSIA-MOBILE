package com.russia.common;


import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class BaseModel {

    public static int INDEX = 0;

    public BaseModel() {
    }

    public BaseModel(Map<String, Object> json) {

        resolve(json);
    }

    public abstract String assemble();

    public abstract void resolve(Map<String, Object> json);


    protected String getColumnList(String[] columns) {
        return Arrays.stream(columns).collect(Collectors.joining(","));
    }

    public abstract void parse(Map<String, Object> json);

}