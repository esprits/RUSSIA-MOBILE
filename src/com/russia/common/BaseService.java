package com.russia.common;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.russia.contract.Endpoints;

import java.io.IOException;

public class BaseService {
    public static final String SEPRATOR = "/";

    private String mBaseUrl;



    public BaseService() {
        this(Endpoints.EndpointEntry.API_BASE_URL);
    }

    public BaseService(String baseUrl) {
        mBaseUrl = baseUrl;
    }

    public byte[] GetResponse(String endPoint, String method) throws IOException {
        System.out.println("Hello");
        ConnectionRequest connection = new ConnectionRequest();

        connection.setUrl(String.format("%s/%s", mBaseUrl, endPoint));
        connection.setPost(false);
        connection.addArgument("format", "json");

        NetworkManager.getInstance().addToQueueAndWait(connection);
        byte[] responseData = connection.getResponseData();

        if (responseData == null) {
            throw new IOException("Network Err");
        }

        return responseData;
    }

    public String getBaseUrl() {
        return mBaseUrl;
    }

    public BaseService setBaseUrl(String mBaseUrl) {
        this.mBaseUrl = mBaseUrl;
        return this;
    }
}
