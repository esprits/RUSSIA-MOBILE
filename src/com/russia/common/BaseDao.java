package com.russia.common;

import java.io.IOException;
import java.util.List;

public interface BaseDao<T> {

    List<T> getAll() throws IOException;

    T get(int id) throws IOException;

}
