package com.russia.contract;

public final class NewsContract {

    private NewsContract() {
    }

    public static abstract class ArticleEntry {
        public static final String _ID = "id";
        public static final String TABLE_NAME = "article";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_CONTENT = "content";
        public static final String COLUMN_NAME_CREATED = "created";
        public static final String COLUMN_NAME_UPDATED = "updated";
        public static final String COLUMN_NAME_SLUG = "slug";
        public static final String COLUMN_NAME_BADGE = "badge_id";
        public static final String COLUMN_NAME_CATEGORY = "category_id";
        public static final String COLUMN_NAME_BADGE_NAME = "badge_name";
        public static final String[] COLUMNS;

        static {
            COLUMNS = new String[]{COLUMN_NAME_TITLE, COLUMN_NAME_CONTENT
                    , COLUMN_NAME_CREATED, COLUMN_NAME_UPDATED, COLUMN_NAME_SLUG, COLUMN_NAME_BADGE, COLUMN_NAME_CATEGORY
            };
        }
    }

    public static abstract class BadgeEntry {
        public static final String _ID = "id";
        public static final String TABLE_NAME = "badge";
        public static final String COLUMN_NAME_CLASS = "class";
        public static final String[] COLUMNS;

        static {
            COLUMNS = new String[]{COLUMN_NAME_CLASS};
        }
    }

    public static abstract class CategoryEntry {
        public static final String _ID = "id";
        public static final String TABLE_NAME = "category";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String[] COLUMNS;

        static {
            COLUMNS = new String[]{COLUMN_NAME_NAME};
        }
    }

    public static abstract class VoteUpEntry {
        public static final String _ID = "id";
        public static final String TABLE_NAME = "vote_up";
        public static final String COLUMN_NAME_ARTICLE = "article_id";
        public static final String COLUMN_NAME_USER = "user_id";
        public static final String COLUMN_NAME_VOTE = "voteUp";
        public static final String[] COLUMNS;

        static {
            COLUMNS = new String[]{
                    COLUMN_NAME_ARTICLE
                    , COLUMN_NAME_USER
                    , COLUMN_NAME_VOTE
            };
        }
    }


}
