package com.russia.contract;

public final class RegionContract {
    private RegionContract() {
    }


    public static abstract class RegionEntry {
        public static final String _ID = "id";
        public static final String TABLE_NAME = "region";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_SLUG = "slug";
        public static final String COLUMN_NAME_IMAGE = "image";
        public static final String COLUMN_NAME_YOUTUBE = "youtube_video";
        public static final String COLUMN_NAME_LOCATION = "location";
        public static final String COLUMN_NAME_PLACES = "places";

        public static final String[] COLUMNS;

        static {
            COLUMNS = new String[]{COLUMN_NAME_NAME
                    , COLUMN_NAME_IMAGE, COLUMN_NAME_YOUTUBE, COLUMN_NAME_LOCATION, COLUMN_NAME_SLUG};
        }

    }
}
