package com.russia.contract;

public final class CategoryContract {
    private CategoryContract() {
    }

    public static abstract class CategoryEntry {
        public static final String _ID = "id";
        public static final String TABLE_NAME = "categorie";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_CODE = "code";
        public static final String COLUMN_NAME_ICON = "icon_type";
        public static final String[] COLUMNS;

        static {
            COLUMNS = new String[]
                    {COLUMN_NAME_NAME, COLUMN_NAME_CODE, COLUMN_NAME_ICON};
        }
    }
}
