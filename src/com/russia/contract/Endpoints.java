package com.russia.contract;

public class Endpoints {
    private Endpoints() {
    }


    public static abstract class EndpointEntry {

        public static final String API_BASE_URL = "http://127.0.0.1:8000/api"; // TODO : ADD your API HERE
        public static final String REGIONS = "regions";
        public static final String PLACES = "places";
        public static final String HOTELS = "hotels";
        public static final String TEAMS = "teams";
        public static final String PLAYERS = "players";
        public static final String SKILLS = "skills";
        public static final String CLUBS = "clubs";
        public static final String VIEWS = "views";
        public static final String TICKET = "tickets";
        public static final String ARTICLE = "article";

    }
}
