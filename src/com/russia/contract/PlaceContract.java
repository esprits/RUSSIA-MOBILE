package com.russia.contract;

public final class PlaceContract {

    private PlaceContract() {
    }

    public static abstract class PlaceEntry {
        public static final String _ID = "id";
        public static final String TABLE_NAME = "place";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_INFORMATION = "information";
        public static final String COLUMN_NAME_TEXT = "preview_text";
        public static final String COLUMN_NAME_PICTURE = "preview_picture";
        public static final String COLUMN_NAME_PHONE = "phone";
        public static final String COLUMN_NAME_URL = "site_url";
        public static final String COLUMN_NAME_REGION = "region";
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_LOCATION = "location";
        public static final String[] COLUMNS;

        static {
            COLUMNS = new String[]{COLUMN_NAME_NAME, COLUMN_NAME_TYPE,
                    COLUMN_NAME_INFORMATION, COLUMN_NAME_TEXT, COLUMN_NAME_PICTURE,
                    COLUMN_NAME_PHONE, COLUMN_NAME_URL, COLUMN_NAME_REGION,
                    COLUMN_NAME_CATEGORY, COLUMN_NAME_LOCATION};
        }
    }
}
