package com.russia.contract;

public final class TicketContract {
    private TicketContract() {
    }


    public static abstract class TicketEntry {
        public static final String _ID = "id";
        public static final String TABLE_NAME = "ticket";
        public static final String COLUMN_NAME_QUANTITY = "quantity";
        public static final String COLUMN_NAME_PRICE = "price";
        public static final String COLUMN_NAME_MATCH = "match_id";

        public static final String[] COLUMNS;

        static {
            COLUMNS =
                    new String[]{COLUMN_NAME_QUANTITY, COLUMN_NAME_PRICE, COLUMN_NAME_MATCH};
        }

    }
}
