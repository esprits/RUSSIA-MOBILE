package com.russia.contract;

public final class LocationContract {

    private LocationContract() {
    }

    public static abstract class LocationEntry {
        public static final String _ID = "id";
        public static final String TABLE_NAME = "location";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_LATITUDE = "latitude";

        public static final String COLUMN_NAME_COUNTRY = "country";
        public static final String COLUMN_NAME_CITY = "city";
        public static final String COLUMN_NAME_STATE = "state";
        public static final String COLUMN_NAME_STREET1 = "street1";
        public static final String COLUMN_NAME_STREET2 = "street2";
        public static final String COLUMN_NAME_POSTAL = "postalcode";

        public static final String[] COLUMNS;

        static {
            COLUMNS = new String[]
                    {};
        }
    }
}
