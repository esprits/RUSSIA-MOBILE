package com.russia.manager;

import com.russia.entities.player.Player;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PlayerManager extends EntityManager<Player> {
    public PlayerManager() {
        super("Player");
    }

    @Override
    public List<Player> getAll(byte[] data) throws IOException {
        Map<String, Object> response = getStringObjectMap(data);

        List<Map<String, Object>> playersJson = (List<Map<String, Object>>) response.get("players");

        return playersJson.stream().map(Player::new).collect(Collectors.toList());
    }

    @Override
    public Player get(byte[] data) throws IOException {

        Map<String, Object> response = getStringObjectMap(data);

        return new Player((Map<String, Object>) response.get("player"));
    }
}
