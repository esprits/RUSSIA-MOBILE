package com.russia.manager;


import com.russia.entities.player.Club;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ClubManager extends EntityManager<Club> {
    public ClubManager() {
        super("Club");
    }

    @Override
    public List<Club> getAll(byte[] data) throws IOException {
        Map<String, Object> response = getStringObjectMap(data);

        List<Map<String, Object>> teamsJson = (List<Map<String, Object>>) response.get("root");

        return teamsJson.stream().map(Club::new).collect(Collectors.toList());
    }

    @Override
    public Club get(byte[] data) throws IOException {

        Map<String, Object> response = getStringObjectMap(data);

        return new Club((Map<String, Object>) ((List) response.get("root")).get(0));
    }
}
