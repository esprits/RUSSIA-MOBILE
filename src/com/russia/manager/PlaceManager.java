package com.russia.manager;

import com.russia.entities.guide.Place;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PlaceManager extends EntityManager<Place> {

    public PlaceManager() {
        super("Region");
    }

    @Override
    public List<Place> getAll(byte[] data) throws IOException {
        Map<String, Object> response = getStringObjectMap(data);

        List<Map<String, Object>> placesJson = (List<Map<String, Object>>) response.get("places");

        return placesJson.stream().map(Place::new).collect(Collectors.toList());
    }

    @Override
    public Place get(byte[] data) throws IOException {

        Map<String, Object> response = getStringObjectMap(data);

        return new Place((Map<String, Object>) ((List) response.get("root")).get(0));
    }


}
