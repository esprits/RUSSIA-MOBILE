package com.russia.manager;

import com.russia.entities.guide.Region;
import com.russia.entities.ticket.Ticket;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class TicketManager extends EntityManager<Ticket> {
    public TicketManager() {
        super("ticket");
    }

    @Override
    public List<Ticket> getAll(byte[] data) throws IOException {
        return null;
    }

    @Override
    public Ticket get(byte[] data) throws IOException {
        Map<String, Object> response = getStringObjectMap(data);


        return new Ticket((Map<String, Object>) ((List) response.get("root")).get(0));
    }
}
