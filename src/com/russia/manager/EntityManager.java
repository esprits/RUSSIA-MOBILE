package com.russia.manager;

import com.codename1.io.JSONParser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

public abstract class EntityManager<T> {

    private String entityName;

    EntityManager(String entityName) {
        this.entityName = entityName;
    }

    public abstract List<T> getAll(byte[] data) throws IOException;
    // Todo Traverse entities


    public abstract T get(byte[] data) throws IOException;
    //todo traverse entity


    protected Map<String, Object> getStringObjectMap(byte[] data) throws IOException {
        JSONParser parser = new JSONParser();
        return parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
    }

    public String getEntityName() {
        return entityName;
    }
}
