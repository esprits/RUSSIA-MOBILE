package com.russia.manager;


import com.russia.entities.player.Skill;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SkillManager extends EntityManager<Skill> {
    public SkillManager() {
        super("Skill");
    }

    @Override
    public List<Skill> getAll(byte[] data) throws IOException {
        Map<String, Object> response = getStringObjectMap(data);

        List<Map<String, Object>> teamsJson = (List<Map<String, Object>>) response.get("root");

        return teamsJson.stream().map(Skill::new).collect(Collectors.toList());
    }

    @Override
    public Skill get(byte[] data) throws IOException {

        Map<String, Object> response = getStringObjectMap(data);

        return new Skill((Map<String, Object>) ((List) response.get("root")).get(0));
    }
}
