package com.russia.manager;

import com.russia.entities.guide.Region;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RegionManager extends EntityManager<Region> {

    public RegionManager() {
        super("Region");
    }

    @Override
    public List<Region> getAll(byte[] data) throws IOException {
        Map<String, Object> response = getStringObjectMap(data);

        List<Map<String, Object>> objects = (List<Map<String, Object>>) response.get("regions"); // ignore this
        return objects.stream().map(Region::new).collect(Collectors.toList());
    }

    @Override
    public Region get(byte[] data) throws IOException {
        Map<String, Object> response = getStringObjectMap(data);

        return new Region((Map<String, Object>) ((List) response.get("root")).get(0));
    }
}
