package com.russia.manager;

import com.russia.entities.team.Team;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TeamManager extends EntityManager<Team> {
    public TeamManager() {
        super("Team");
    }

    @Override
    public List<Team> getAll(byte[] data) throws IOException {
        Map<String, Object> response = getStringObjectMap(data);

        List<Map<String, Object>> teamsJson = (List<Map<String, Object>>) response.get("teams");

        return teamsJson.stream().map(Team::new).collect(Collectors.toList());
    }

    @Override
    public Team get(byte[] data) throws IOException {

        Map<String, Object> response = getStringObjectMap(data);

        return new Team((Map<String, Object>) response.get("team"));
    }
}
